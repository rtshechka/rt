/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects_parser.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 16:41:30 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 13:57:34 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECTS_PARSER_H
# define OBJECTS_PARSER_H

# include "raytracer.h"
# include "parser.h"
# include "geometry_primitives.h"
# include "opencllib.h"

void	parse_sphere(t_file *file, t_object *obj);
void	parse_plane(t_file *file, t_object *obj);
void	parse_cylinder(t_file *file, t_object *obj);
void	parse_cone(t_file *file, t_object *obj);
void	parse_disk(t_file *file, t_object *obj);

int		help1(t_object *obj, char **const arr);

float	get_radian(float angle);

/*
** Cone
*/

int		cone_intersection(const t_object *obj, const t_line *ray, float *t);
int		cone_rotate(int key, t_object *obj, float move);

/*
** Cylinder
*/

int		cylinder_intersection(const t_object *obj, const t_line *ray, float *t);
int		cyl_rotate(int key, t_object *obj, float move);

/*
** Plane
*/

int		plane_intersection(const t_object *obj, const t_line *ray, float *t);
int		plane_rotate(int key, t_object *obj, float move);

/*
** Disk
*/

int		disk_intersection(const t_object *obj, const t_line *ray, float *t);
int		disk_rotate(int key, t_object *obj, float move);

/*
** Sphere
*/

int		sphere_intersection(const t_object *obj, const t_line *ray, float *t);
int		sphere_rotate(int key, t_object *obj, float move);

/*
** Light
*/

int		light_help1(t_light *light, char **const arr);

void	parse_direct(t_file *file, t_light *light);
void	parse_dot(t_file *file, t_light *light);
void	parse_ambient(t_file *file, t_light *light);
void	parse_projector(t_file *file, t_light *light);

#endif
