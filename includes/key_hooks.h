/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hooks.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 19:50:03 by bcherkas          #+#    #+#             */
/*   Updated: 2018/09/12 17:52:15 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEY_HOOKS_H
# define KEY_HOOKS_H

# ifdef __APPLE__

#  define KEY_ESCAPE 53
#  define KEY_DEL 117

#  define BRACKET_LEFT 33
#  define BRACKET_RIGHT 30

#  define ARROW_LEFT 123
#  define ARROW_UP 126
#  define ARROW_RIGHT 124
#  define ARROW_DOWN 125

#  define NUMPAD_MIN 78
#  define NUMPAD_PLUS 69

#  define KEY_SPACE 49
#  define KEY_LSHIFT 257
#  define KEY_RSHIFT 258
#  define KEY_TAB 48

#  define KEY_0 29
#  define KEY_1 18
#  define KEY_2 19
#  define KEY_3 20
#  define KEY_4 21
#  define KEY_5 23
#  define KEY_6 22
#  define KEY_7 26
#  define KEY_8 28
#  define KEY_9 25

#  define NUMPAD_1 83
#  define NUMPAD_2 84
#  define NUMPAD_3 85
#  define NUMPAD_4 86
#  define NUMPAD_5 87
#  define NUMPAD_6 88
#  define NUMPAD_7 89
#  define NUMPAD_8 91
#  define NUMPAD_9 92
#  define NUMPAD_0 82

#  define KEY_A 0
#  define KEY_B 11
#  define KEY_C 8
#  define KEY_D 2
#  define KEY_E 14
#  define KEY_F 3
#  define KEY_G 5
#  define KEY_H 4
#  define KEY_I 34
#  define KEY_J 38
#  define KEY_K 40
#  define KEY_L 37
#  define KEY_M 46
#  define KEY_N 45
#  define KEY_O 31
#  define KEY_P 35
#  define KEY_Q 12
#  define KEY_R 15
#  define KEY_S 1
#  define KEY_T 17
#  define KEY_U 32
#  define KEY_V 9
#  define KEY_W 13
#  define KEY_X 7
#  define KEY_Y 16
#  define KEY_Z 6

# else

#  define KEY_0 48
#  define KEY_1 49
#  define KEY_2 50
#  define KEY_3 51
#  define KEY_4 52
#  define KEY_5 53
#  define KEY_6 54
#  define KEY_7 55
#  define KEY_8 56
#  define KEY_9 57

#  define NUMPAD_1 65436
#  define NUMPAD_2 65433
#  define NUMPAD_3 65435
#  define NUMPAD_4 65430
#  define NUMPAD_5 65437
#  define NUMPAD_6 65432
#  define NUMPAD_7 65429
#  define NUMPAD_8 65431
#  define NUMPAD_9 65434
#  define NUMPAD_0 65438

#  define ARROW_LEFT 65361
#  define ARROW_UP 65362
#  define ARROW_RIGHT 65363
#  define ARROW_DOWN 65364

#  define KEY_ESCAPE 65307
#  define KEY_DEL 65535

#  define KEY_SPACE 32
#  define KEY_LSHIFT 65505
#  define KEY_RSHIFT 65506
#  define KEY_TAB 65289

#  define BRACKET_LEFT 91
#  define BRACKET_RIGHT 93

#  define KEY_MIN 45
#  define KEY_PLUS 61
#  define NUMPAD_MIN 65453
#  define NUMPAD_PLUS 65451

#  define KEY_A 97
#  define KEY_B 98
#  define KEY_C 99
#  define KEY_D 100
#  define KEY_E 101
#  define KEY_F 102
#  define KEY_G 103
#  define KEY_H 104
#  define KEY_I 105
#  define KEY_J 106
#  define KEY_K 107
#  define KEY_L 108
#  define KEY_M 109
#  define KEY_N 110
#  define KEY_O 111
#  define KEY_P 112
#  define KEY_Q 113
#  define KEY_R 114
#  define KEY_S 115
#  define KEY_T 116
#  define KEY_U 117
#  define KEY_V 118
#  define KEY_W 119
#  define KEY_X 120
#  define KEY_Y 121
#  define KEY_Z 122

# endif
#endif
