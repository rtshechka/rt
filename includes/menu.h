/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:53:16 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 15:54:16 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_MENU_H
# define RTV1_MENU_H

static const int	g_params_start = 10;

static const int	g_values_start = 30;

static const int	g_skip = 40;

static const int	g_params_skip = 15;

static const int	g_values_skip = 30;

static const int	g_values_length = g_values_skip + 30;

static const int	g_plus_place = 70;

static const int	g_min_place = 30;

static const int	g_plus_min_len = 20;

static const int	g_obj_parameters = 3;

#endif
