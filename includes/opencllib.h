/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opencllib.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/28 15:28:27 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:07:24 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPENCLLIB_H
# define OPENCLLIB_H

# define CL_USE_DEPRECATED_OPENCL_1_2_APIS

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
# else
#  include <CL/cl.h>
# endif

# define MAX_SOURCE_SIZE 100000

typedef struct			s_devices
{
	cl_platform_id		platform_id;
	cl_device_id		device_cpu;
	cl_device_id		device_gpu;
	cl_uint				num_platforms;
	cl_uint				num_devices_cpu;
	cl_uint				num_devices_gpu;
}						t_dev;

typedef struct			s_cl
{
	char				*source_str;
	size_t				source_size;
	cl_device_id		device_id;
	cl_context			context;
	cl_command_queue	command_queue;
	cl_program			program;
	cl_kernel			kernel;
}						t_cl;

t_dev					*cl_create_device_list(void);
t_cl					*cl_create_context(cl_device_id device,
							cl_uint num_devices);
void					cl_create_program(t_cl *cl, char *filename,
							cl_device_id devices);
void					cl_clear(t_cl *cl);
void					print_compile_errors(t_cl *cl, cl_device_id *device_id);
int						cl_error_check(t_cl *cl, cl_int ret);

#endif
