/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   geometry_primitives.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 19:04:15 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:52:15 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GEOMETRY_PRIMITIVES_H
# define GEOMETRY_PRIMITIVES_H

# define EPSILON 0.001f

# define SPHERE 1
# define PLANE 2
# define CYLINDER 3
# define CONE 4
# define DISK 5

# define DIRECT_LIGHT 1
# define AMBIENT_LIGHT 2

# define TOON_EFFECT 1

# include "opencllib.h"
# include "libft.h"

typedef struct	s_line
{
	cl_float4	start;
	cl_float4	ray;
	cl_float	len;
}				t_line;

typedef struct	s_object
{
	cl_float4	center;
	cl_float4	axis;
	cl_float4	normal;
	cl_float4	m;
	cl_int4		color;
	cl_float	tg;
	cl_float	pow_radius;
	cl_float	reflection;
	cl_float	transparency;
	cl_float	refraction;
	cl_int		specularity;
	cl_int		texture;
	cl_int		type;
}				t_object;

typedef struct	s_cam
{
	cl_float4	point;
	cl_float4	rotation;
	cl_float	screen_distance;
	cl_float	screen_width;
	cl_float	screen_height;
	cl_float	rel_x;
	cl_float	rel_y;
	cl_int		map_x;
	cl_int		map_y;
}				t_cam;

typedef struct	s_light
{
	cl_float4	position;
	cl_int4		color;
	cl_float	intensity;
	cl_int		type;
}				t_light;

cl_float4		new_vector(float x, float y, float z);
cl_int4			new_color(int red, int green, int blue);
t_line			new_line(cl_float4 start, cl_float4 direction);
t_line			new_line2(cl_float4 start, cl_float4 direction);

float			ray_product(cl_float4 vect1, cl_float4 vect2);
float			ray_pow(cl_float4 vect);
float			ray_length(cl_float4 vect);
float			ray_cos(cl_float4 vect1, cl_float4 vect2);
cl_float4		ray_normalize(cl_float4 vect);

cl_float4		ray_add(cl_float4 ray, float num);
cl_float4		ray_sub(cl_float4 ray, float num);
cl_float4		ray_mult(cl_float4 ray, float num);
cl_float4		ray_div(cl_float4 ray, float num);

cl_float4		vector_add(cl_float4 ray, cl_float4 num);
cl_float4		vector_sub(cl_float4 ray, cl_float4 num);
cl_float4		vector_mult(cl_float4 ray, cl_float4 num);
cl_float4		vector_div(cl_float4 ray, cl_float4 num);

#endif
