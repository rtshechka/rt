/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 18:01:17 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:37:58 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "libft.h"
# include "geometry_primitives.h"
# include "get_next_line.h"
# include "ft_printf.h"

typedef struct		s_file
{
	cl_int4			ambient_light_color;
	t_list			*obj_stack;
	t_list			*light_stack;
	t_cam			*camera;
	char			*scene_name;
	float			ambient_light;
	int				reflection;
	int				magic_reflections;
	int				scene_size_x;
	int				scene_size_y;
	int				effect;
	int				fd;
}					t_file;

t_file				*parsing(char *str);

void				scene_parser(t_file *file);
void				light_parser(t_file *file);
void				objects_parser(t_file *file);

void				scene_parser_help(t_file *file, char *str);

char				*trim_str(char *str);
char				*get_line(int fd);
char				**get_line_arr(int fd, int len);
void				checkarrlen(char **arr, int len);

int					len2arr(char **arr);
void				free2arr(char **arr);

void				errmsg(const char *str);

void				sorttokens(t_list **objs);

#endif
