/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytracer.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 19:49:48 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 13:36:57 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAYTRACER_H
# define RAYTRACER_H

# include "libft.h"
# include "ft_printf.h"
# include "mlx.h"
# include "key_hooks.h"
# include "opencllib.h"

# include "geometry_primitives.h"

# define CAMERAMOVE 0.1f
# define OBJECTMOVE 0.15f
# define ROTATE 0.1f
# define MOUSEMOVE 0.005f

# define ONOFF(x) (((x) == 0 ? "OFF" : "ON"))

const static int	g_menu_length = 300;

typedef struct	s_img
{
	void			*img_ptr;
	int				*img_arr;
	int				pixel_mass;
	int				line_mass;
	int				img_mass;
	int				endi;
}				t_img;

typedef struct	s_info
{
	cl_mem			mem_obj;
	cl_mem			mem_obj2;
	cl_mem			mem_obj3;
	cl_int4			ambient_light_color;
	t_cl			*cl;
	t_cam			*camera;
	t_img			*image;
	t_img			*menu;
	t_object		*obj_arr;
	t_light			*light_arr;
	void			*mlxptr;
	void			*winptr;
	t_object		*chosen_obj;
	char			*scene_name;
	cl_float		ambient_light;
	cl_int			num_objs;
	cl_int			num_lights;
	cl_int			num_pixels;
	cl_int			recursion;
	cl_int			samples;
	cl_int			magic_reflections;
	cl_int			interactive_mode;
	cl_int			effect;
	cl_int			help_mode;
	cl_int			direct_light;
}				t_info;

typedef struct	s_helper
{
	cl_int4			ambient_light_color;
	cl_int			recursion;
	cl_int			magic_reflections;
	cl_int			effect;
	cl_int			samples;
	cl_int			direct_light;
}				t_helper;

typedef struct	s_menu
{
	int				*value;
	float			*val;
	char			*str;
}				t_menu;

/*
** MLX
*/

int				mlx_starts(int (*trigger_func)(), t_info *inf);

int				triggers(int key, void *p);
int				mousepress(int button, int x, int y, void *p);
int				camera_move(int key, t_cam *camera, float move);
int				camera_rotate(int key, t_cam *camera, float rotate);

int				mouse_move(int x, int y, void *p);

int				mouse_obj_move(int x, int y, void *p);

/*
** Camera
*/

cl_float4		view3d(t_cam *camera, int y, int x);

/*
** Render
*/

void			main_loop(t_info *inf);

int				intersect_point(const t_line *ray, t_object *obj,
					t_object **save);

/*
** Object constructors
*/

t_light			new_lightsource(cl_float4 position, float intensity);

t_object		new_sphere(float radius, cl_float4 center_point, cl_int4 color);
t_object		new_plane(cl_float4 point, cl_float4 normal, cl_int4 color);
t_object		new_cylinder(float radius, cl_float4 center, cl_float4 axis,
					cl_int4 color);
t_object		new_cone(cl_float4 center, cl_float4 axis, cl_int4 color,
					float angle);

/*
** Math functions
*/

float			get_radian(float angle);
int				sq_equation(cl_float4 k, float *res);

cl_float4		rotate_vector(cl_float4 vector, cl_float4 rotate);

/*
** Help functions
*/

void			sorttokens(t_list **objs);

void			del_obj(void *content, size_t size);
void			errmsg(const char *str);

void			convert_to_array(t_list *obj, t_list *light, t_info *inf);

/*
** Menu
*/

int				plusmin_value(int *d, float *f, int x, int i);

int				change_sample(int *d, int move);

int				fillmenu(t_info *inf, t_img *menu);
int				fillmenu_obj(t_info *inf, int i);
int				triggermenu(t_info *inf, int y, int x);
void			triggermenu_obj(t_info *inf, int y, int x);

#endif
