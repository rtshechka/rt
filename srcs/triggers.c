/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triggers.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 18:11:47 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 13:46:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "objects_parser.h"
#include "geometry_primitives.h"

void		choose_rotate(int key, t_object *obj, float rotate, t_info *inf)
{
	if (!inf->interactive_mode)
		return ;
	if (inf->interactive_mode == 0)
		return ;
	if (obj->type == SPHERE)
		sphere_rotate(key, obj, rotate);
	else if (obj->type == PLANE)
		plane_rotate(key, obj, rotate);
	else if (obj->type == CYLINDER)
		cyl_rotate(key, obj, rotate);
	else if (obj->type == CONE)
		cone_rotate(key, obj, rotate);
	else if (obj->type == DISK)
		disk_rotate(key, obj, rotate);
	return ;
}

void		move_object(int key, t_object *obj, float move, t_info *inf)
{
	cl_float4	vector;

	if (!inf->interactive_mode)
		return ;
	vector = new_vector(0, 0, 0);
	if (key == ARROW_DOWN || key == ARROW_LEFT || key == NUMPAD_PLUS)
		move *= -1;
	if (key == ARROW_RIGHT || key == ARROW_LEFT)
		vector.x = move;
	else if (key == ARROW_UP || key == ARROW_DOWN)
		vector.y = move;
	else if (key == NUMPAD_MIN || key == NUMPAD_PLUS)
		vector.z = move;
	else
		return ;
	vector = rotate_vector(vector, inf->camera->rotation);
	obj->center = vector_add(obj->center, vector);
	main_loop(NULL);
	return ;
}

int			camera_move_rotate(int key, t_info *inf)
{
	if (key == KEY_J || key == KEY_K || key == KEY_L || key == KEY_I ||
			key == KEY_N || key == KEY_M)
		return (camera_rotate(key, inf->camera, ROTATE));
	else if (key == KEY_A || key == KEY_D || key == KEY_S || key == KEY_W ||
			key == KEY_SPACE || key == KEY_Z)
		return (camera_move(key, inf->camera, CAMERAMOVE));
	return (0);
}

int			change_mode(t_info *inf, int key)
{
	if (key == KEY_B)
		inf->interactive_mode ^= 1;
	else if (key == KEY_H)
		inf->help_mode ^= 1;
	else if (key == KEY_V)
	{
		inf->direct_light ^= 1;
		main_loop(NULL);
	}
	return (1);
}

int			triggers(int key, void *p)
{
	t_info *const	inf = (t_info *)p;

	if (inf->chosen_obj && (key == ARROW_LEFT || key == ARROW_RIGHT ||
			key == ARROW_UP || key == ARROW_DOWN || key == NUMPAD_MIN ||
				key == NUMPAD_PLUS))
		move_object(key, inf->chosen_obj, OBJECTMOVE, inf);
	else if (inf->chosen_obj && (key == NUMPAD_2 || key == NUMPAD_4 ||
			key == NUMPAD_6 || key == NUMPAD_8 || key == NUMPAD_7 ||
				key == NUMPAD_9))
		choose_rotate(key, inf->chosen_obj, OBJECTMOVE, inf);
	else if (key == KEY_B || key == KEY_H || key == KEY_V)
		change_mode(inf, key);
	else
		camera_move_rotate(key, inf);
	fillmenu(inf, inf->menu);
	return (0);
}
