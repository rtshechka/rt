/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu_object.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 19:00:38 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 14:45:12 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "menu.h"

static const int	g_params_num = 3;

static const int	g_values_num = 3;

static char			*g_params[g_params_num] = {
	"Object reflection",
	"Object transparency",
	"Object refraction",
};

static t_menu		g_values[g_params_num];

static void			init_menu_values(t_object *obj)
{
	static char	plusmin[6] = "-   +\0";

	g_values[0].val = &obj->reflection;
	g_values[0].str = plusmin;
	g_values[1].val = &obj->transparency;
	g_values[1].str = plusmin;
	g_values[2].val = &obj->refraction;
	g_values[2].str = plusmin;
}

void				triggermenu_obj(t_info *inf, int y, int x)
{
	int		i;
	int		save;

	i = g_values_num + 2;
	save = i + 1;
	while (++i < save + g_values_num)
	{
		if (y >= (g_values_start + i * g_skip) &&
				y <= (g_values_start + i * g_skip + g_skip / 2))
		{
			plusmin_value(NULL, g_values[i - save].val, x, 0);
			fillmenu(inf, inf->menu);
			main_loop(NULL);
			return ;
		}
	}
}

int					fillmenu_obj(t_info *inf, int i)
{
	int		save;

	save = i;
	i--;
	init_menu_values(inf->chosen_obj);
	while (++i < save + g_params_num)
		mlx_string_put(inf->mlxptr, inf->winptr, g_params_skip,
				g_params_start + i * g_skip, 0xFFFFFF, g_params[i - save]);
	i = save - 1;
	while (++i < save + g_params_num)
		mlx_string_put(inf->mlxptr, inf->winptr, g_values_skip,
				g_values_start + i * g_skip, 0xFFFFFF, g_values[i - save].str);
	return (0);
}

int					change_sample(int *d, int move)
{
	*d += move;
	if (*d < 1)
		*d = 1;
	else if (*d > 2 && move == -1)
		*d = 2;
	else if (*d > 2 && move == 1)
		*d = 4;
	else if (*d > 4)
		*d = 4;
	return (0);
}
