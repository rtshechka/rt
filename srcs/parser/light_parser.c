/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_parser.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 19:44:45 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:51:51 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "objects_parser.h"

const static int	g_len = 2;

static void			(*g_funcs[g_len])(t_file *, t_light *) =
{parse_direct, parse_ambient};

static char			*(g_names[g_len]) =
{"direct:", "ambient:"};

static t_light		init_light(int fd)
{
	t_light			light;
	char *const		str = get_line(fd);

	if (!ft_strequ(str, "["))
		errmsg("Wrong light representation");
	light.color = new_color(255, 255, 255);
	light.intensity = 0;
	free(str);
	return (light);
}

static int			choose_way(t_file *file, char *str)
{
	int			i;
	t_light		light;
	t_list		*lst;

	i = -1;
	while (++i < g_len)
		if (ft_strequ(str, g_names[i]))
		{
			light = init_light(file->fd);
			g_funcs[i](file, &light);
			if (i != g_len - 1)
			{
				if (!(lst = ft_lstnew(&light, sizeof(t_object))))
					errmsg("Cannot allocate memory");
				ft_lstadd(&(file->light_stack), lst);
			}
			return (0);
		}
	if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	else
		errmsg("Wrong infomation in light parser");
	return (0);
}

void				light_parser(t_file *file)
{
	char		*str;

	file->ambient_light = 0;
	file->ambient_light_color = new_color(255, 255, 255);
	while (1)
	{
		str = get_line(file->fd);
		if (choose_way(file, str))
		{
			free(str);
			break ;
		}
		free(str);
	}
}
