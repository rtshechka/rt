/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 17:47:45 by bcherkas          #+#    #+#             */
/*   Updated: 2018/10/31 17:12:36 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <fcntl.h>

static int		open_file(char *str)
{
	char	**arr;
	int		len;
	int		fd;

	if (!(arr = ft_strsplit(str, '.')))
		errmsg("Cannot allocate memory");
	len = len2arr(arr);
	if (len < 2 || !ft_strequ(arr[len - 1], "rt"))
		errmsg("Wrong filename. It must be like \"Name.rt\"");
	free2arr(arr);
	if ((fd = open(str, O_RDONLY)) < 3)
		errmsg("Cannot open file");
	return (fd);
}

static t_file	*initialization(char *str)
{
	t_file	*file;

	if (!(file = (t_file *)malloc(sizeof(t_file))))
		errmsg("Cannot allocate memory");
	file->reflection = 0;
	file->magic_reflections = 0;
	file->effect = 0;
	file->obj_stack = NULL;
	file->light_stack = NULL;
	file->camera = (t_cam *)malloc(sizeof(t_cam));
	if (file->camera == NULL)
		errmsg("Cannot allocate memory");
	file->fd = open_file(str);
	return (file);
}

static void		choose_parser(t_file *file)
{
	char	*str;

	str = get_line(file->fd);
	if (ft_strequ(str, "scene:"))
		scene_parser(file);
	else
		errmsg("Scene parser must be first");
	free(str);
	str = get_line(file->fd);
	if (ft_strequ(str, "objects:"))
		objects_parser(file);
	else
		errmsg("Objects parser must be second");
	free(str);
	str = get_line(file->fd);
	if (ft_strequ(str, "light:"))
		light_parser(file);
	else
		errmsg("Light parser must be last");
	free(str);
}

t_file			*parsing(char *str)
{
	t_file	*file;
	char	*tmp;

	file = initialization(str);
	choose_parser(file);
	if (get_next_line(file->fd, &tmp))
		errmsg("Wrong file");
	close(file->fd);
	sorttokens(&file->obj_stack);
	return (file);
}
