/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sphere.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 19:23:08 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:38:54 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"
#include <math.h>

static int	sphere_lines(t_file *file, int *save, t_object *obj)
{
	char	**const	arr = get_line_arr(file->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		checkarrlen(arr, 4);
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "radius"))
	{
		checkarrlen(arr, 2);
		obj->pow_radius = pow(ft_atof(arr[1]), 2);
		*save |= 0b10;
	}
	else
		ret = help1(obj, arr);
	free2arr(arr);
	return (ret);
}

void		parse_sphere(t_file *file, t_object *obj)
{
	int			save;

	save = 0;
	while (sphere_lines(file, &save, obj))
		;
	if (save != 0b11)
		errmsg("You have to type more information to create sphere");
	obj->type = SPHERE;
}
