/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 16:31:52 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:48:26 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "geometry_primitives.h"
#include "libft.h"

void		splitlist(t_list *src, t_list **p1, t_list **p2)
{
	t_list	*fast;
	t_list	*slow;

	if (src == NULL || src->next == NULL)
	{
		*p1 = src;
		return ;
	}
	slow = src;
	fast = src->next;
	while (fast)
	{
		fast = fast->next;
		if (fast)
		{
			slow = slow->next;
			fast = fast->next;
		}
	}
	*p1 = src;
	*p2 = slow->next;
	slow->next = NULL;
}

int			compare(t_list *p1, t_list *p2)
{
	if (((t_object *)p1->content)->transparency <
			((t_object *)p2->content)->transparency)
		return (1);
	return (0);
}

t_list		*mergesorted(t_list *p1, t_list *p2)
{
	t_list	*ret;

	if (!p1)
		return (p2);
	else if (!p2)
		return (p1);
	ret = NULL;
	if (compare(p1, p2))
	{
		ret = p1;
		ret->next = mergesorted(p1->next, p2);
	}
	else
	{
		ret = p2;
		ret->next = mergesorted(p1, p2->next);
	}
	return (ret);
}

void		sorttokens(t_list **objs)
{
	t_list	*head;
	t_list	*p1;
	t_list	*p2;

	head = *objs;
	if (head == NULL || head->next == NULL)
		return ;
	p1 = NULL;
	p2 = NULL;
	splitlist(head, &p1, &p2);
	sorttokens(&p1);
	sorttokens(&p2);
	*objs = mergesorted(p1, p2);
}
