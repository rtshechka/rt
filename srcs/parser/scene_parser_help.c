/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_parser_help.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 16:41:58 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:50:13 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static void		parse_effects(t_file *file)
{
	char *const	str = get_line(file->fd);

	if (file->effect > 0)
		errmsg("Scene can have only one effect");
	if (ft_strequ(str, "cartoon"))
		file->effect = TOON_EFFECT;
}

static void		parse_reflection(t_file *file)
{
	char *const	str = get_line(file->fd);

	file->reflection = ft_atoi(str);
	if (file->reflection < 0 || file->reflection > 9)
		errmsg("Reflection reflection depth must be between 0 and 9");
	free(str);
}

static void		parse_magic_reflections(t_file *file)
{
	char *const	str = get_line(file->fd);
	int			i;

	i = 0;
	while (str[i])
	{
		str[i] = ft_tolower(str[i]);
		i++;
	}
	if (ft_strequ(str, "on"))
		file->magic_reflections = 1;
	free(str);
}

void			scene_parser_help(t_file *file, char *str)
{
	if (ft_strequ(str, "reflection depth:"))
		parse_reflection(file);
	else if (ft_strequ(str, "magic reflections:"))
		parse_magic_reflections(file);
	else if (ft_strequ(str, "effect:"))
		parse_effects(file);
	else
		errmsg("Wrong information in scene parser");
}
