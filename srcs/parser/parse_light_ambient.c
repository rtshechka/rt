/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_light_ambient.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/04 16:07:13 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:52:39 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

int				ambient_lines(t_file *file)
{
	char **const	arr = get_line_arr(file->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "]"))
		ret = 0;
	else if (ft_strequ(arr[0], "brightness"))
	{
		checkarrlen(arr, 2);
		file->ambient_light = ft_atof(arr[1]);
		if (file->ambient_light < 0 || file->ambient_light > 1)
			errmsg("Light intensity can have value from 0 to 1");
	}
	else if (ft_strequ(arr[0], "color"))
	{
		checkarrlen(arr, 4);
		file->ambient_light_color = new_color(ft_atoi(arr[1]),
				ft_atoi(arr[2]), ft_atoi(arr[3]));
	}
	else
		errmsg("Wrong information in ambient light representation");
	free2arr(arr);
	return (ret);
}

void			parse_ambient(t_file *file, t_light *light)
{
	if (light)
		;
	while (ambient_lines(file))
		;
}
