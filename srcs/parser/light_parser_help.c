/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_parser_help.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 16:35:45 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:51:13 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry_primitives.h"
#include "raytracer.h"
#include "parser.h"

int				light_help1(t_light *light, char **const arr)
{
	if (ft_strequ(arr[0], "]"))
		return (0);
	if (ft_strequ(arr[0], "brightness"))
	{
		checkarrlen(arr, 2);
		light->intensity = ft_atof(arr[1]);
		if (light->intensity < 0 || light->intensity > 1)
			errmsg("Light intensity can have value from 0 to 1");
	}
	else if (ft_strequ(arr[0], "color"))
	{
		checkarrlen(arr, 4);
		light->color = new_color(ft_atoi(arr[1]), ft_atoi(arr[2]),
				ft_atoi(arr[3]));
	}
	return (1);
}
