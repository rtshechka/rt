/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects_parser_help.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 19:19:28 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:43:15 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

static void	help2(t_object *obj, char **const arr)
{
	if (ft_strequ(arr[0], "reflection"))
	{
		checkarrlen(arr, 2);
		obj->reflection = ft_atof(arr[1]);
		if (obj->reflection < 0 || obj->reflection > 1)
			errmsg("Reflection of objects can be between 0 and 1");
	}
	else if (ft_strequ(arr[0], "transparency"))
	{
		checkarrlen(arr, 2);
		obj->transparency = ft_atof(arr[1]);
		if (obj->transparency < 0 || obj->transparency > 1)
			errmsg("Reflection of objects can be between 0 and 1");
	}
	else if (ft_strequ(arr[0], "refraction"))
	{
		checkarrlen(arr, 2);
		obj->refraction = ft_atof(arr[1]);
	}
	else
		errmsg("Wrong information in object representation");
}

int			help1(t_object *obj, char **const arr)
{
	if (ft_strequ(arr[0], "]"))
		return (0);
	if (ft_strequ(arr[0], "rgb"))
	{
		checkarrlen(arr, 4);
		obj->color = new_color(ft_atoi(arr[1]), ft_atoi(arr[2]),
					ft_atoi(arr[3]));
		if (obj->color.x > 255 || obj->color.y > 255 ||
				obj->color.z > 255 || obj->color.x < 0 ||
				obj->color.y < 0 || obj->color.z < 0)
			errmsg("Color cannot be bigger than 255 or lesser than 0");
	}
	else if (ft_strequ(arr[0], "specularity"))
	{
		checkarrlen(arr, 2);
		obj->specularity = ft_atof(arr[1]);
		if (obj->specularity != -1 && obj->specularity < 0)
			errmsg("Specularity of objects can be 0 and bigger, or -1");
	}
	else
		help2(obj, arr);
	return (1);
}
