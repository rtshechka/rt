/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects_parser.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 18:25:44 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 15:01:48 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

const static int	g_len = 5;

static void			(*g_funcs[g_len])(t_file *, t_object *) =
{parse_plane, parse_sphere, parse_cylinder, parse_cone, parse_disk};

static char			*(g_names[g_len]) =
{"plane:", "sphere:", "cylinder:", "cone:", "disk:"};

static t_object	init_object(int fd)
{
	t_object	obj;
	char *const str = get_line(fd);

	if (!ft_strequ(str, "["))
		errmsg("Wrong object representation");
	obj.color = new_color(255, 255, 255);
	obj.reflection = 0;
	obj.transparency = 0;
	obj.refraction = 1;
	obj.specularity = -1;
	obj.texture = -1;
	free(str);
	return (obj);
}

static int		choose_way(t_file *file, char *str)
{
	t_object	obj;
	t_list		*lst;
	int			i;

	i = -1;
	while (++i < g_len)
		if (ft_strequ(str, g_names[i]))
		{
			obj = init_object(file->fd);
			g_funcs[i](file, &obj);
			if (!(lst = ft_lstnew(&obj, sizeof(t_object))))
				errmsg("Cannot allocate memory");
			ft_lstadd(&(file->obj_stack), lst);
			return (0);
		}
	if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	else
		errmsg("Wrong information in objects parser");
	return (0);
}

void			objects_parser(t_file *file)
{
	char		*str;

	while (1)
	{
		str = get_line(file->fd);
		if (choose_way(file, str))
		{
			free(str);
			break ;
		}
		free(str);
	}
}
