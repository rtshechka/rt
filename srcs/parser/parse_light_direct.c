/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_light_direct.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 15:58:59 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:52:53 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

static int		direct_lines(t_file *file, t_light *light, int *save)
{
	char **const	arr = get_line_arr(file->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "position"))
	{
		checkarrlen(arr, 4);
		light->position = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
				ft_atof(arr[3]));
		*save = 1;
	}
	else
		ret = light_help1(light, arr);
	free2arr(arr);
	return (ret);
}

void			parse_direct(t_file *file, t_light *light)
{
	int		save;

	save = 0;
	while (direct_lines(file, light, &save))
		;
	if (save != 1)
		errmsg("Light position missing");
	light->type = DIRECT_LIGHT;
}
