/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_funcs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 18:34:40 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:36:43 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

char	*trim_str(char *str)
{
	char	*trim;

	if (str == NULL)
		return (NULL);
	if (!(trim = ft_strtrim(str)))
		errmsg("Cannot allocate memory");
	free(str);
	return (trim);
}

char	*get_line(int fd)
{
	char	*str;

	str = NULL;
	if ((get_next_line(fd, &str)) < 1)
		errmsg("Wrong file");
	if (!(str = trim_str(str)))
		errmsg("Wrong file");
	while (str && (str[0] == 0 || str[0] == '#'))
	{
		free(str);
		str = NULL;
		if ((get_next_line(fd, &str)) < 1)
			errmsg("Wrong file");
		if (!(str = trim_str(str)))
			errmsg("Wrong file");
	}
	return (str);
}

char	**get_line_arr(int fd, int len)
{
	char	**arr;
	char	*str;

	str = get_line(fd);
	arr = ft_strsplit(str, ' ');
	if (arr == NULL)
		errmsg("Cannot allocate memory");
	if (len < len2arr(arr))
		errmsg("Wrong file");
	free(str);
	return (arr);
}

void	checkarrlen(char **arr, int len)
{
	if (len2arr(arr) < len)
		errmsg("Too small amount of params");
}
