/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cylinder.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 19:19:37 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:42:29 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"
#include <math.h>

static int	obj_lines2(char **arr, int *save, t_object *obj)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "length"))
	{
		checkarrlen(arr, 2);
		obj->m.x = ft_atof(arr[1]);
		if (obj->m.x < 0.1f || obj->m.x > 1000)
			errmsg("Cylinder length cannot be lesser than 0.1 / "
					"or bigger than 1000");
	}
	else if (ft_strequ(arr[0], "axis"))
	{
		checkarrlen(arr, 4);
		obj->axis = ray_normalize(new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3])));
		*save |= 0b100;
	}
	else
		ret = help1(obj, arr);
	return (ret);
}

static int	cylinder_lines(t_file *file, int *save, t_object *obj)
{
	char	**const	arr = get_line_arr(file->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		checkarrlen(arr, 4);
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "radius"))
	{
		checkarrlen(arr, 2);
		obj->pow_radius = pow(ft_atof(arr[1]), 2);
		*save |= 0b10;
	}
	else
		ret = obj_lines2(arr, save, obj);
	free2arr(arr);
	return (ret);
}

void		parse_cylinder(t_file *file, t_object *obj)
{
	int			save;

	save = 0;
	obj->m = new_vector(-1, -1, -1);
	while (cylinder_lines(file, &save, obj))
		;
	if (save != 0b111)
		errmsg("Need more information to create cylinder");
	obj->type = CYLINDER;
}
