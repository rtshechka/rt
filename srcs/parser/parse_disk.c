/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_disk.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 13:30:35 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:41:28 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"
#include <math.h>

static int	disk_lines2(t_object *obj, char **arr)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "radius"))
	{
		checkarrlen(arr, 2);
		obj->pow_radius = ft_atof(arr[1]);
		obj->pow_radius = pow(obj->pow_radius, 2);
	}
	else
		ret = help1(obj, arr);
	return (ret);
}

static int	disk_lines(t_file *file, int *save, t_object *obj)
{
	int		ret;
	char	**arr;

	ret = 1;
	arr = get_line_arr(file->fd, 4);
	if (ft_strequ(arr[0], "origin"))
	{
		checkarrlen(arr, 4);
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
				ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "normal"))
	{
		checkarrlen(arr, 4);
		obj->normal = ray_normalize(new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3])));
		*save |= 0b10;
	}
	else
		ret = disk_lines2(obj, arr);
	free2arr(arr);
	return (ret);
}

void		parse_disk(t_file *file, t_object *obj)
{
	int			save;

	save = 0;
	obj->pow_radius = 3;
	while (disk_lines(file, &save, obj))
		;
	if (save != 0b11)
		errmsg("Need more information to allocate disk");
	obj->type = DISK;
}
