/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene_parser.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 18:25:44 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:55:08 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

static void		parse_camera(t_file *file)
{
	char	**arr;
	int		i;

	i = 0;
	while (i < 2)
	{
		arr = get_line_arr(file->fd, 4);
		checkarrlen(arr, 4);
		if (ft_strequ(arr[0], "origin"))
			file->camera->point = new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3]));
		else if (ft_strequ(arr[0], "rotation"))
			file->camera->rotation =
				new_vector(ft_atof(arr[1]), ft_atof(arr[2]), ft_atof(arr[3]));
		else
			errmsg("Wrong file");
		free2arr(arr);
		i++;
	}
}

static void		parse_render(t_file *file)
{
	char	**arr;

	arr = get_line_arr(file->fd, 3);
	if (ft_strequ(arr[0], "size"))
	{
		checkarrlen(arr, 3);
		file->scene_size_x = ft_atoi(arr[1]);
		file->scene_size_y = ft_atoi(arr[2]);
		if (file->scene_size_x < 600 || file->scene_size_x > 1920)
			errmsg("Scene height can't be lesser than 600 or bigger than 1920");
		if (file->scene_size_y < 600 || file->scene_size_y > 1368)
			errmsg("Scene width can't be lesser than 600 or bigger than 1368");
	}
	else
		errmsg("Wrong file");
	free2arr(arr);
}

static int		choose_way(t_file *file, char *str, int *save)
{
	if (ft_strequ(str, "camera:"))
	{
		*save ^= 0b001;
		parse_camera(file);
	}
	else if (ft_strequ(str, "render:"))
	{
		*save ^= 0b010;
		parse_render(file);
	}
	else if (ft_strequ(str, "name:"))
	{
		*save ^= 0b100;
		file->scene_name = get_line(file->fd);
	}
	else if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	else
		scene_parser_help(file, str);
	return (0);
}

void			scene_parser(t_file *file)
{
	char		*str;
	int			save;

	save = 0;
	while (1)
	{
		str = get_line(file->fd);
		if (choose_way(file, str, &save))
		{
			free(str);
			break ;
		}
		free(str);
	}
	if (save != 0b111)
		errmsg("Do not have enough information to create scene");
}
