/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_plane.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 19:21:36 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:39:09 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

static int	plane_lines(t_file *file, int *save, t_object *obj)
{
	int		ret;
	char	**arr;

	ret = 1;
	arr = get_line_arr(file->fd, 4);
	if (ft_strequ(arr[0], "origin"))
	{
		checkarrlen(arr, 4);
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
				ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "normal"))
	{
		checkarrlen(arr, 4);
		obj->normal = ray_normalize(new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3])));
		*save |= 0b10;
	}
	else
		ret = help1(obj, arr);
	free2arr(arr);
	return (ret);
}

void		parse_plane(t_file *file, t_object *obj)
{
	int			save;

	save = 0;
	while (plane_lines(file, &save, obj))
		;
	if (save != 0b11)
		errmsg("Need more information to allocate plane");
	obj->type = PLANE;
}
