/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cone.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 19:20:27 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 19:42:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"
#include <math.h>

static int	cone_lines3(char **arr, int *save, t_object *obj)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "axis"))
	{
		checkarrlen(arr, 4);
		obj->axis = ray_normalize(new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3])));
		*save |= 0b100;
	}
	else
		ret = help1(obj, arr);
	return (ret);
}

static int	cone_lines2(char **arr, int *save, t_object *obj)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "upperlength"))
	{
		checkarrlen(arr, 2);
		obj->m.y = ft_atof(arr[1]);
		if (obj->m.y < 0 || obj->m.z > 1000)
			errmsg("Cone length can be from 0 to 1000");
	}
	else if (ft_strequ(arr[0], "lowerlength"))
	{
		checkarrlen(arr, 2);
		obj->m.z = ft_atof(arr[1]);
		if (obj->m.y < 0 || obj->m.z > 1000)
			errmsg("Cone length can be from 0 to 1000");
	}
	else
		ret = cone_lines3(arr, save, obj);
	return (ret);
}

static int	cone_lines(t_file *file, int *save, t_object *obj)
{
	char	**const	arr = get_line_arr(file->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		checkarrlen(arr, 4);
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "angle"))
	{
		checkarrlen(arr, 2);
		obj->tg = ft_atoi(arr[1]);
		if (obj->tg > 178 || obj->tg < 1)
			errmsg("Cone angle must be 1-178");
		obj->tg = pow(tan(get_radian(obj->tg / 2)), 2);
		*save |= 0b10;
	}
	else
		ret = cone_lines2(arr, save, obj);
	free2arr(arr);
	return (ret);
}

void		parse_cone(t_file *file, t_object *obj)
{
	int			save;

	save = 0;
	obj->m = new_vector(-1, -1, -1);
	while (cone_lines(file, &save, obj))
		;
	if (save != 0b111)
		errmsg("Need more information to create cone");
	obj->type = CONE;
}
