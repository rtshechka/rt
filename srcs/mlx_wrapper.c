/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_wrapper.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:22 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 15:59:11 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	exitwindow(void *elem)
{
	t_info	*inf;

	inf = (t_info *)elem;
	mlx_destroy_window(inf->mlxptr, inf->winptr);
	exit(1);
	return (0);
}

static int	escapewindow(int key, void *elem)
{
	t_info	*inf;

	if (key == KEY_ESCAPE)
	{
		inf = (t_info *)elem;
		mlx_destroy_window(inf->mlxptr, inf->winptr);
		exit(1);
	}
	return (0);
}

static int	create_image(int map_x, int map_y, t_img *image, void *mlxptr)
{
	image->pixel_mass = 8;
	image->line_mass = image->pixel_mass * map_x;
	image->img_ptr = mlx_new_image(mlxptr, map_x, map_y);
	if (image->img_ptr == NULL)
		return (1);
	image->endi = 0;
	image->img_mass = map_x * map_y;
	image->img_arr = (int *)mlx_get_data_addr(image->img_ptr,
		&(image->pixel_mass), &(image->line_mass), &(image->endi));
	if (image->img_arr == NULL)
		return (1);
	return (0);
}

int			mlx_starts(int (*trigger_func)(), t_info *inf)
{
	inf->mlxptr = mlx_init();
	if (inf->mlxptr == NULL)
		return (1);
	inf->winptr = mlx_new_window(inf->mlxptr, g_menu_length +
			inf->camera->map_x, inf->camera->map_y, inf->scene_name);
	if (inf->winptr == NULL)
		return (1);
	mlx_hook(inf->winptr, 17, 1L << 17, exitwindow, (void *)inf);
	mlx_key_hook(inf->winptr, escapewindow, (void *)inf);
	mlx_hook(inf->winptr, 2, 5, trigger_func, (void *)inf);
	if ((inf->image = (t_img *)malloc(sizeof(t_img))) == NULL)
		return (1);
	if (create_image(inf->camera->map_x, inf->camera->map_y,
			inf->image, inf->mlxptr))
		return (1);
	if ((inf->menu = (t_img *)malloc(sizeof(t_img))) == NULL)
		return (1);
	if (create_image(g_menu_length, inf->camera->map_y,
			inf->menu, inf->mlxptr))
		return (1);
	return (0);
}
