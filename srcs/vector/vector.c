/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   geom_primitives.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:03 by bcherkas          #+#    #+#             */
/*   Updated: 2018/10/18 17:42:53 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry_primitives.h"

cl_float4		new_vector(float x, float y, float z)
{
	cl_float4	vector;

	vector.x = x;
	vector.y = y;
	vector.z = z;
	return (vector);
}

t_line			new_line(cl_float4 start, cl_float4 direction)
{
	t_line	line;

	line.ray = ray_normalize(vector_sub(direction, start));
	line.len = ray_pow(line.ray);
	line.start = vector_add(start, ray_mult(line.ray, EPSILON));
	return (line);
}

t_line			new_line2(cl_float4 start, cl_float4 direction)
{
	t_line	line;

	line.start = vector_add(start,
			ray_mult(ray_normalize(vector_sub(direction, start)), EPSILON));
	line.ray = vector_sub(direction, start);
	line.len = ray_pow(line.ray);
	return (line);
}
