/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_operations1.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 18:41:02 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:41:05 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_float4	vector_add(cl_float4 vector, cl_float4 num)
{
	return (new_vector(vector.x + num.x, vector.y + num.y, vector.z + num.z));
}

cl_float4	vector_sub(cl_float4 vector, cl_float4 num)
{
	return (new_vector(vector.x - num.x, vector.y - num.y, vector.z - num.z));
}

cl_float4	vector_mult(cl_float4 vector, cl_float4 num)
{
	return (new_vector(vector.x * num.x, vector.y * num.y, vector.z * num.z));
}

cl_float4	vector_div(cl_float4 vector, cl_float4 num)
{
	return (new_vector(vector.x / num.x, vector.y / num.y, vector.z / num.z));
}
