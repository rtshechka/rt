/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 17:39:18 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:40:49 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

float		ray_product(cl_float4 vect1, cl_float4 vect2)
{
	return (vect1.x * vect2.x + vect1.y * vect2.y + vect1.z * vect2.z);
}

float		ray_pow(cl_float4 vect)
{
	return ((vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z));
}

float		ray_length(cl_float4 vect)
{
	float	ret;

	ret = (vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z);
	return (sqrt(ret));
}

float		ray_cos(cl_float4 vect1, cl_float4 vect2)
{
	float	save;

	if ((save = ray_product(vect1, vect2)) <= 0)
		return (-1);
	save /= ray_length(vect1);
	save /= ray_length(vect2);
	return (save);
}

cl_float4	ray_normalize(cl_float4 vect)
{
	const float	len = ray_length(vect);

	if (len < 0.00001 && len > -0.00001)
		return (new_vector(0, 0, 0));
	vect.x /= len;
	vect.y /= len;
	vect.z /= len;
	return (vect);
}
