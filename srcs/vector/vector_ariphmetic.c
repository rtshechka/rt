/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_ariphmetic.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 14:33:58 by bcherkas          #+#    #+#             */
/*   Updated: 2018/08/06 15:02:35 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_float4	ray_add(cl_float4 ray, float num)
{
	return (new_vector(ray.x + num, ray.y + num, ray.z + num));
}

cl_float4	ray_sub(cl_float4 ray, float num)
{
	return (new_vector(ray.x - num, ray.y - num, ray.z - num));
}

cl_float4	ray_mult(cl_float4 ray, float num)
{
	return (new_vector(ray.x * num, ray.y * num, ray.z * num));
}

cl_float4	ray_div(cl_float4 ray, float num)
{
	if (num > 0 || num < 0)
		return (new_vector(ray.x / num, ray.y / num, ray.z / num));
	return (ray);
}
