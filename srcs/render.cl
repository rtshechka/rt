/*
** Protopypes
*/

# define SPHERE 1
# define PLANE 2
# define CYLINDER 3
# define CONE 4
# define DISK 5

# define DIRECT_LIGHT 1
# define DOT_LIGHT 2
# define PROJECTOR_LIGHT 3
# define AMBIENT_LIGHT 4

# define TOON_EFFECT 1

# define NULL ((void *)0)

static __constant const int     max_recursion = 3;
static __constant const float   EPSILON = 0.00001f;

typedef struct	s_line
{
	float4	    start;
	float4	    ray;
	float	    len;
}				t_line;

typedef struct	s_saved_obj
{
	t_line		    ray;
	float4		    intersection_point;
	float4		    normal;
	__global struct s_object	*obj;
	float		    distance;
	int             recursion;
}				t_saved_obj;

typedef struct	s_object
{
	float4	    center;
	float4	    axis;
	float4	    normal;
	float4	    m;
	int4		color;
	float	    tg;
	float	    pow_radius;
	float       reflection;
	float       transparency;
	float       refraction;
	int		    specularity;
	int         texture;
	int		    type;
}				t_object;

typedef struct	s_light
{
	float4	    position;
	int4		color;
	float	    intensity;
	int		    type;
}				t_light;

typedef struct	s_cam
{
	float4	    point;
	float4	    rotation;
	float	    screen_distance;
	float	    screen_width;
	float	    screen_height;
	float	    rel_x;
	float	    rel_y;
	int		    map_x;
	int		    map_y;
}				t_cam;

typedef struct  s_helper
{
	int4        ambient_light_color;
	int         recursion;
	int         magic_reflections;
	int         effect;
	int			samples;
	int			direct_light;
}               t_helper;

float4		    new_vector(float x, float y, float z);
t_line			new_line(float4 start, float4 direction);
t_line			new_line2(float4 start, float4 direction);

float4	        ray_add(float4 ray, float num);
float4	        ray_sub(float4 ray, float num);
float4	        ray_mult(float4 ray, float num);
float4	        ray_div(float4 ray, float num);

float		    ray_product(float4 vect1, float4 vect2);
float		    ray_pow(float4 vect);
float		    ray_length(float4 vect);
float		    ray_cos(float4 vect1, float4 vect2);
float4	        ray_normalize(float4 vect);

float4	        vector_add(float4 vector, float4 num);
float4	        vector_sub(float4 vector, float4 num);
float4	        vector_mult(float4 vector, float4 num);
float4	        vector_div(float4 vector, float4 num);

float4	        rotate_z(float4 rotate, float4 ray);
float4	        rotate_xy(float4 rotate, float4 ray);
float4		    rotate_vector(float4 vector, float4 rotate);

int4            new_color(int red, int green, int blue);
int             myfloor(int num, float rel);
int4            cut_rgb(int4 rgb, int4 limit);
int		        rgb_to_int(int4 c);
int4            mult_color(int4 color, float mult);

int4	        mult_lights(int4 obj_color, int4 light_color, float obj_mult, float light_mult);
int4	        mult_colors(int4 obj_color, int4 light_color, int4 spec_col);
int4            mix_colors(int4 color, int4 color2);

int4			sample(__global __read_only t_object *objs, __global __read_only t_light *lights, t_cam const cam, float ambient_light, t_helper helper, float y, float x);

int				choose_intersect(__global t_object *obj, const t_line *ray, float *t);
float4			choose_normal(t_saved_obj *save);
int4            choose_color(t_saved_obj const *const save);

float           use_effect(t_helper helper, float coef, float ambient_light);

int4			get_pixel_color(t_saved_obj *save, __global t_object *objs, __global t_light *light, const float ambient_light, t_helper help);

int4			direct_light(t_line *ray, __global t_object *objs, __global t_light *light);

float	        get_shadow(const t_line ray, __global t_object *obj, __global t_object *save);

float		    diffuse(const float4 intersection_point, const float4 normal, __global const t_light *light);
float		    specular(const float4 normal, const float4 ray_direction, __global const t_light *light, const t_saved_obj *save);

float4		    view3d(const t_cam *cam, float y, float x);

int				cone_intersection(__global const t_object *obj, const t_line *ray, float *t);
float4			cone_normal(t_saved_obj const *const save);
int4            cone_color(t_saved_obj const *const save);

int				cylinder_intersection(__global const t_object *obj, const t_line *ray, float *t);
float4			cyl_normal(t_saved_obj const *const save);
int4            cyl_color(t_saved_obj const *const save);

int				plane_intersection(__global const t_object *obj, const t_line *ray, float *t);
float4			plane_normal(t_saved_obj const *const save);
int4            plane_color(t_saved_obj const *const save);

int				disk_intersection(__global const t_object *obj, const t_line *ray, float *t);
float4			disk_normal(t_saved_obj const *const save);
int4            disk_color(t_saved_obj const *const save);

int				sphere_intersection(__global const t_object *obj, const t_line *ray, float *t);
float4			sphere_normal(t_saved_obj const *const save);
int4            sphere_color(t_saved_obj const *const save);

int		        sq_equation(float4 k, float *res);
int				intersect_point(const t_line *ray, __global t_object *obj, t_saved_obj *save, __global t_object *ignore);

int4            reflection(t_saved_obj *save, t_helper helper, __global __read_only t_object *objs, __global __read_only t_light *lights, float ambient_light, int rec, int max_rec);

t_line			refraction(t_saved_obj save, t_line line);

int4            transparency(t_saved_obj save, t_helper help, t_line line,  __global __read_only t_object *objs, __global __read_only t_light *lights, float ambient_light, int4 color);

t_line	        reflect_ray(float4 ray, t_saved_obj *save);
int4	        return_color(t_helper helper, int4 *color, t_saved_obj *save, int i);

/*
** vector and lines
*/

float4		    new_vector(float x, float y, float z)
{
	float4	vector;

	vector.x = x;
	vector.y = y;
	vector.z = z;
	return (vector);
}

t_line			new_line(float4 start, float4 direction)
{
	t_line	line;

	line.ray = ray_normalize(direction - start);
	line.start = start + line.ray * EPSILON;
	line.len = ray_pow(line.ray);
	return (line);
}

t_line			new_line2(float4 start, float4 direction)
{
	t_line	line;

	line.ray = direction - start;
	line.start = start + (ray_normalize(line.ray) * EPSILON);
	line.len = ray_pow(line.ray);
	return (line);
}


float4	    ray_add(float4 ray, float num)
{
	return (new_vector(ray.x + num, ray.y + num, ray.z + num));
}

float4	    ray_sub(float4 ray, float num)
{
	return (new_vector(ray.x - num, ray.y - num, ray.z - num));
}

float4	    ray_mult(float4 ray, float num)
{
	return (new_vector(ray.x * num, ray.y * num, ray.z * num));
}

float4	    ray_div(float4 ray, float num)
{
	if (num > 0 || num < 0)
		return (new_vector(ray.x / num, ray.y / num, ray.z / num));
	return (ray);
}


float		ray_product(float4 vect1, float4 vect2)
{
	return (vect1.x * vect2.x + vect1.y * vect2.y + vect1.z * vect2.z);
}

float		ray_pow(float4 vect)
{
	return ((vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z));
}

float		ray_length(float4 vect)
{
	float	ret;

	ret = (vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z);
	return (sqrt(ret));
}

float		ray_cos(float4 vect1, float4 vect2)
{
	float	save;

	if ((save = ray_product(vect1, vect2)) <= 0)
		return (-1);
	save /= ray_length(vect1);
	save /= ray_length(vect2);
	return (save);
}

float4	    ray_normalize(float4 vect)
{
	const float	len = ray_length(vect);

	if (len < 0.00001 && len > -0.00001)
		return (new_vector(0, 0, 0));
	vect.x /= len;
	vect.y /= len;
	vect.z /= len;
	return (vect);
}

float4	    vector_add(float4 vector, float4 num)
{
	return (new_vector(vector.x + num.x, vector.y + num.y, vector.z + num.z));
}

float4	    vector_sub(float4 vector, float4 num)
{
	return (new_vector(vector.x - num.x, vector.y - num.y, vector.z - num.z));
}

float4	    vector_mult(float4 vector, float4 num)
{
	return (new_vector(vector.x * num.x, vector.y * num.y, vector.z * num.z));
}

float4	    vector_div(float4 vector, float4 num)
{
	return (new_vector(vector.x / num.x, vector.y / num.y, vector.z / num.z));
}


float4	    rotate_z(float4 rotate, float4 ray)
{
	const float	cos_z = cos(rotate.z);
	const float	sin_z = sin(rotate.z);

	rotate.x = (ray.x * cos_z) - (ray.y * sin_z);
	rotate.y = (ray.x * sin_z) + (ray.y * cos_z);
	rotate.z = ray.z;
	return (rotate);
}

float4	    rotate_xy(float4 rotate, float4 ray)
{
	const float	cos_x = cos(rotate.x);
	const float	sin_x = sin(rotate.x);
	const float	cos_y = cos(rotate.y);
	const float	sin_y = sin(rotate.y);

	rotate.y = (ray.y * cos_x) - (ray.z * sin_x);
	rotate.z = (ray.y * sin_x) + (ray.z * cos_x);
	rotate.x = ray.x;
	ray = rotate;
	rotate.x = (ray.x * cos_y) + (ray.z * sin_y);
	rotate.z = (-ray.x * sin_y) + (ray.z * cos_y);
	rotate.y = ray.y;
	return (rotate);
}

float4		rotate_vector(float4 vector, float4 rotate)
{
	vector = rotate_xy(rotate, vector);
	vector = rotate_z(rotate, vector);
	return (vector);
}


/*
** Colors
*/

int         myfloor(int num, float rel)
{
	return ((int)floor(num * rel));
}

int4        mult_color(int4 color, float mult)
{
	color.x = (int)(floor(color.x * mult));
	color.y = (int)(floor(color.y * mult));
	color.z = (int)(floor(color.z * mult));
	return (color);
}

int4        cut_rgb(int4 rgb, int4 limit)
{
	if (rgb.x > limit.x)
		rgb.x = limit.x;
	else if (rgb.x < 0)
		rgb.x = 0;
	if (rgb.y > limit.y)
		rgb.y = limit.y;
	else if (rgb.y < 0)
		rgb.y = 0;
	if (rgb.z > limit.z)
		rgb.z = limit.z;
	else if (rgb.z < 0)
		rgb.z = 0;
	return (rgb);
}

int4        new_color(int red, int green, int blue)
{
	int4     color;

	color.x = red;
	color.y = green;
	color.z = blue;
	return (color);
}

int4	    mult_lights(int4 obj_color, int4 light_color, float obj_mult, float light_mult)
{
	const float	mult = obj_mult * (1.0f - light_mult);

	obj_color.x = myfloor(obj_color.x, mult) + myfloor(light_color.x, light_mult);
	obj_color.y = myfloor(obj_color.y, mult) + myfloor(light_color.y, light_mult);
	obj_color.z = myfloor(obj_color.z, mult) + myfloor(light_color.z, light_mult);
	obj_color = cut_rgb(obj_color, new_color(255, 255, 255));
	return (obj_color);
}

int4	    mult_colors(int4 obj_color, int4 light_color, int4 spec_color)
{
	int4        tmp = cut_rgb(light_color, obj_color);

	obj_color = tmp + spec_color;
	obj_color = cut_rgb(obj_color, new_color(255, 255, 255));
	return (obj_color);
}

int4        mix_colors(int4 color, int4 color2)
{
	float4      tmp = new_vector(color.x / 255.0f, color.y / 255.0f, color.z / 255.0f);

	color.x += (int)floor(color2.x * (1.0f - tmp.x));
	color.y += (int)floor(color2.y * (1.0f - tmp.y));
	color.z += (int)floor(color2.z * (1.0f - tmp.z));
	return (color);
}

int		        rgb_to_int(int4 c)
{
	return ((c.x << 16) | (c.y << 8) | c.z);
}

/*
** Light and shadow
*/

/*
** i_s is intension and specularity
*/

float           use_effect(t_helper helper, float coef, float ambient_light)
{
	if (helper.effect == TOON_EFFECT)
	{
		if (coef + ambient_light < 0.4)
			coef = 0.3f;
		else if (coef + ambient_light < 0.7)
			coef = 1.0f;
		else
			coef = 1.3f;
	}
	return (coef);
}

int4			get_pixel_color(t_saved_obj *save, __global t_object *objs, __global t_light *light, const float ambient_light, t_helper helper)
{
	float			    i_s[2];
	float               shadow = 1;
	int4                color = mult_color(helper.ambient_light_color, ambient_light);
	int4                spec_col = new_color(0, 0, 0);

	i_s[0] = 0.0;
	i_s[1] = 0.0;
	save->normal = choose_normal(save);
	while (light->type > 0)
	{
		shadow = get_shadow(new_line2(save->intersection_point, light->position), objs, save->obj);
		if (shadow > 0)
		{
			i_s[0] = diffuse(save->intersection_point, save->normal, light) * shadow;
			i_s[1] = specular(save->normal, save->ray.ray, light, save) * shadow;
			if (helper.effect > 0)
				i_s[0] = use_effect(helper, i_s[0], ambient_light);
			color = mix_colors(color, mult_color(light->color, i_s[0]));
			spec_col = mix_colors(spec_col, mult_color(light->color, i_s[1]));
		}
		light++;
	}
	return (mult_colors(choose_color(save), color, spec_col));
}

float	        get_shadow(const t_line ray, __global t_object *obj, __global t_object *save)
{
	float			distance;

	while (obj->type)
	{
		if (save != obj && choose_intersect(obj, &ray, &distance) && (1 - distance > EPSILON) && distance > EPSILON)
			return (obj->transparency);
		obj++;
	}
	return (1);
}

float		diffuse(const float4 intersection_point, const float4 normal, __global const t_light *light)
{
	float4	    light_ray;
	float		intensity;

	light_ray = vector_sub(light->position, intersection_point);
	intensity = ray_cos(normal, light_ray);
	if (intensity <= 0)
		return (0);
	intensity = light->intensity * intensity;
	return (intensity);
}

float		specular(const float4 normal, const float4 ray_direction, __global const t_light *light, const t_saved_obj *save)
{
	float4	    light_vector;
	float4	    mid_vector;
	float		res;

	if (save->obj->specularity < 0)
		return (0);
	light_vector = vector_sub(light->position, save->intersection_point);
	mid_vector = vector_sub(ray_mult(normal, 2 * ray_product(normal, light_vector)), light_vector);
	res = ray_cos(ray_mult(ray_direction, -1), mid_vector);
	if (res <= 0)
		return (0);
	res = light->intensity * pown(res, save->obj->specularity);
	return (res);
}

/*
** Direct light
*/

int4			direct_light(t_line *ray, __global t_object *objs, __global t_light *light)
{
	float			    i_s;
	float               shadow = 1;
	int4                color = new_color(0, 0, 0);

	i_s = 0.0f;
	while (light->type > 0)
	{
		shadow = get_shadow(new_line2(ray->start, light->position), objs, NULL);
		if (shadow > 0)
		{
			i_s = diffuse(ray->start, ray->ray, light) * shadow;
			color = mix_colors(color, mult_color(light->color, i_s));
		}
		light++;
	}
	return (color);
}

/*
** Object choosers
*/

int				choose_intersect(__global t_object *obj, const t_line *ray, float *t)
{
	if (obj->type == SPHERE)
		return (sphere_intersection(obj, ray, t));
	else if (obj->type == PLANE)
		return (plane_intersection(obj, ray, t));
	else if (obj->type == CYLINDER)
		return (cylinder_intersection(obj, ray, t));
	else if (obj->type == CONE)
		return (cone_intersection(obj, ray, t));
	else if (obj->type == DISK)
		return (disk_intersection(obj, ray, t));
	return (0);
}


float4			choose_normal(t_saved_obj *save)
{
	if (save->obj->type == SPHERE)
		return (sphere_normal(save));
	else if (save->obj->type == PLANE)
		return (plane_normal(save));
	else if (save->obj->type == CYLINDER)
		return (cyl_normal(save));
	else if (save->obj->type == CONE)
		return (cone_normal(save));
	else if (save->obj->type == DISK)
		return (disk_normal(save));
	return (new_vector(0, 0, 0));
}

int4        choose_color(t_saved_obj const *const save)
{
	if (!save || !save->obj)
		return (new_color(0, 0, 0));
	if (save->obj->type == SPHERE)
		return (sphere_color(save));
	else if (save->obj->type == PLANE)
		return (plane_color(save));
	else if (save->obj->type == CYLINDER)
		return (cyl_color(save));
	else if (save->obj->type == CONE)
		return (cone_color(save));
	else if (save->obj->type == DISK)
		return (disk_color(save));
	return (new_color(0, 0, 0));
}

/*
** Intersection
*/

float4      view3d(const t_cam *cam, float y, float x)
{
	float4		vector;

	vector.x = (-cam->map_x / 2 + x) * cam->rel_x;
	vector.y = (cam->map_y / 2 - y) * cam->rel_y;
	vector.z = cam->screen_distance;
	vector = rotate_vector(vector, cam->rotation);
	vector = vector_add(vector, cam->point);
	return (vector);
}

int		    sq_equation(float4 k, float *res)
{
	float	t[2];
	float	discriminant;
	float	tmp;

	discriminant = pow(k.y, 2) - 4 * k.x * k.z;
	if (discriminant < 0)
		return (0);
	else if (discriminant > 0)
		discriminant = sqrt(discriminant);
	t[0] = (-k.y - discriminant) / k.x / 2;
	t[1] = (-k.y + discriminant) / k.x / 2;
	if (t[0] < 0 && t[1] < 0)
		return (0);
	*res = (t[1] - t[0] > EPSILON) ? t[0] : t[1];
	tmp = (t[0] - t[1] > EPSILON) ? t[0] : t[1];
	if ((tmp - *res) > EPSILON && *res < 0)
		*res = tmp;
	return (1);
}


int			cone_intersection(const __global t_object *obj, const t_line *ray, float *t)
{
	float4		    k;
	float           m;
	const float		r_axis = ray_product(ray->ray, obj->axis);
	const float		cam_cone_axis = ray_product(vector_sub(ray->start, obj->center), obj->axis);

	k.x = ray->len - (1 + obj->tg) * pow(r_axis, 2);
	k.y = 2 * (ray_product(ray->ray, vector_sub(ray->start, obj->center)) - (obj->tg + 1) * r_axis * cam_cone_axis);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - (obj->tg + 1) * pow(cam_cone_axis, 2);
	if (!sq_equation(k, t))
		return (0);
	if (obj->m.y < 0 && obj->m.z < 0)
		return (1);
	m = r_axis * (*t) + cam_cone_axis;
	if ((m <= obj->m.y || obj->m.y < 0) && (m >= -obj->m.z || obj->m.z < 0))
		return (1);
	return (0);
}

float4		cone_normal(t_saved_obj const *const save)
{
	__global t_object *const	obj = save->obj;
	float4		normal;
	const float		r_axis = ray_product(save->ray.ray, obj->axis);
	const float		cam_cone_axis = ray_product(vector_sub(save->ray.start, obj->center), obj->axis);

	normal = obj->center + ray_mult(obj->axis, (obj->tg + 1) * (r_axis * save->distance + cam_cone_axis));
	return (ray_normalize(save->intersection_point - normal));
}

int4        cone_color(t_saved_obj const *const save)
{
	if (save && save->obj)
		return (save->obj->color);
	return (new_color(0, 0, 0));
}

int			cylinder_intersection(const __global t_object *obj, const t_line *ray, float *t)
{
	float               m;
	float4			    k;
	const float			r_axis = ray_product(ray->ray, obj->axis);
	const float			cam_cyl_axis = ray_product(vector_sub(ray->start, obj->center), obj->axis);

	k.x = ray->len - pow(r_axis, 2);
	k.y = 2 * (ray_product(ray->ray, vector_sub(ray->start, obj->center)) - (r_axis * cam_cyl_axis));
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - obj->pow_radius - pow(cam_cyl_axis, 2);
	if (!sq_equation(k, t))
		return (0);
	if (obj->m.x < 0)
		return (1);
	m = r_axis * (*t) + cam_cyl_axis;
	if (m > obj->m.x || m < -obj->m.x)
		return (0);
	return (1);
}

float4		cyl_normal(t_saved_obj const *const save)
{
	__global t_object *const		obj = save->obj;
	float4			normal;
	const float			r_axis = ray_product(save->ray.ray, obj->axis);
	const float			cam_cyl_axis =

			ray_product(save->ray.start - obj->center, obj->axis);
	normal = vector_add(obj->center, ray_mult(obj->axis, r_axis * save->distance + cam_cyl_axis));
	return (ray_normalize(save->intersection_point - normal));
}

int4        cyl_color(t_saved_obj const *const save)
{
	if (save && save->obj)
		return (save->obj->color);
	return (new_color(0, 0, 0));
}

int			plane_intersection(const __global t_object *obj, const t_line *ray, float *t)
{
	const float		d_v = ray_product(ray->ray, obj->normal);
	float			x_v;

	if (d_v == 0)
		return (0);
	x_v = ray_product(obj->center - ray->start, obj->normal);
	if ((d_v < 0 ? -1 : 1) != (x_v < 0 ? -1 : 1))
		return (0);
	*t = x_v / d_v;
	return (1);
}

float4		plane_normal(t_saved_obj const *const save)
{
	__global t_object *const	obj = save->obj;

	if (ray_product(save->ray.ray, obj->normal) > 0)
		return (ray_mult(obj->normal, -1));
	return (obj->normal);
}

int4        plane_color(t_saved_obj const *const save)
{
	if (save && save->obj)
		return (save->obj->color);
	return (new_color(0, 0, 0));
}

int			disk_intersection(const __global t_object *obj, const t_line *ray, float *t)
{
	const float		d_v = ray_product(ray->ray, obj->normal);
	float			x_v;
	float4			intersection_point;

	if (d_v == 0)
		return (0);
	x_v = ray_product(obj->center - ray->start, obj->normal);
	if ((d_v < 0 ? -1 : 1) != (x_v < 0 ? -1 : 1))
		return (0);
	*t = x_v / d_v;
	intersection_point = ray->start + (ray_mult(ray->ray, *t));
	x_v = ray_pow(intersection_point - obj->center);
	if (x_v <= obj->pow_radius)
		return (1);
	return (0);
}

float4		disk_normal(t_saved_obj const *const save)
{
	__global t_object *const	obj = save->obj;

	if (ray_product(save->ray.ray, obj->normal) > 0)
		return (ray_mult(obj->normal, -1));
	return (obj->normal);
}

int4        disk_color(t_saved_obj const *const save)
{
	if (save && save->obj)
		return (save->obj->color);
	return (new_color(0, 0, 0));
}

int			sphere_intersection(const __global t_object *obj, const t_line *ray, float *t)
{
	const float4		cam_sphere = ray->start - obj->center;
	float4			k;

	k.x = ray->len;
	k.y = 2 * ray_product(ray->ray, cam_sphere);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - obj->pow_radius;
	if (!sq_equation(k, t))
		return (0);
	return (1);
}

float4		sphere_normal(t_saved_obj const *const save)
{
	__global t_object *const	obj = save->obj;

	return (ray_normalize(vector_sub(save->intersection_point, obj->center)));
}

int4        sphere_color(t_saved_obj const *const save)
{
	if (save && save->obj)
		return (save->obj->color);
	return (new_color(0, 0, 0));
}

int			intersect_point(const t_line *ray, __global t_object *obj, t_saved_obj *save, __global t_object *ignore)
{
	float		d;

	d = 0;
	save->distance = INFINITY;
	save->obj = NULL;
	while (obj->type)
	{
		if (obj != ignore)
		{
			if (choose_intersect(obj, ray, &d) && d > 0 && (save->distance - d > EPSILON))
			{
				save->distance = d;
				save->obj = obj;
			}
		}
		obj++;
	}
	if (save->obj == NULL)
		return (0);
	save->intersection_point = vector_add(ray->start, (ray_mult(ray->ray, save->distance)));
	return (1);
}

/*
*  Tracing ray
*/

t_line	        reflect_ray(float4 ray, t_saved_obj *save)
{
	t_line		line;

	ray = ray_mult(ray, -1);
	line.start = save->intersection_point;
	line.ray = ray_mult(save->normal, 2 * ray_product(save->normal, ray)) - ray;
	line.len = ray_pow(line.ray);
	return (line);
}

int4            reflection(t_saved_obj *save, t_helper helper, __global __read_only t_object *objs, __global __read_only t_light *lights, float ambient_light, int rec, int max_rec)
{

	int4                color = new_color(0, 0, 0);
	t_line              line;
	__global t_object   *ignore = NULL;

	line = reflect_ray(save->ray.ray, save);
	ignore = save->obj;
	save->ray = line;
	if (intersect_point(&line, objs, save, ignore))
	{
		color = get_pixel_color(save, objs, lights, ambient_light, helper);
		if (rec == max_rec && save->obj->reflection)
			color = mult_color(color, 1 - save->obj->reflection);
	}
	return (color);
}

t_line			refraction(t_saved_obj save, t_line line)
{
	float       rel = 2.0f - save.obj->refraction;
	float		cosi = ray_product(line.ray, save.normal);

	line.start = save.intersection_point + ray_mult(line.ray, 0.01);
	line.ray = ray_mult(line.ray, rel) - ray_mult(save.normal, rel * cosi - cosi);
	line.len = ray_pow(line.ray);
	return (line);
}

int4            transparency(t_saved_obj save, t_helper helper, t_line line, __global __read_only t_object *objs, __global __read_only t_light *lights, float ambient_light, int4 color)
{
	int4		new_rgb = new_color(0, 0, 0);
	float       transparency_coef = save.obj->transparency;
	int         rec = -1;

	while (rec < helper.recursion && save.obj && save.obj->transparency)
	{
		if (save.obj->refraction != 1)
			line = refraction(save, line);
		else
			line.start = save.intersection_point;
		save.ray = line;
		transparency_coef *= save.obj->transparency;
		if (intersect_point(&line, objs, &save, save.obj))
		{
			new_rgb = get_pixel_color(&save, objs, lights, ambient_light, helper);
			if (helper.recursion > 0 && save.obj->reflection)
			{
				float reflection_coef = save.obj->reflection;
				int4 color2 = new_color(0, 0, 0);

				color2 = reflection(&save, helper, objs, lights, ambient_light, rec, helper.recursion);
				if (!helper.magic_reflections)
					new_rgb = mult_lights(new_rgb, color2, 1, reflection_coef);
				else {
					int rec2 = 0;

					while (1) {
						new_rgb = mult_lights(color2, new_rgb, 1, reflection_coef);
						if (rec2 >= helper.recursion || !save.obj || !save.obj->reflection)
							break;
						reflection_coef *= save.obj->reflection;
						++rec2;
					}
				}
			}
		}
		color = mult_lights(color, new_rgb, 1, transparency_coef);
		++rec;
	}
	return (color);
}

int4		sample(__global __read_only t_object *objs, __global __read_only t_light *lights, t_cam const cam, float ambient_light, t_helper helper, float y, float x)
{
	t_line      line = new_line(cam.point, view3d(&cam, y, x));

	t_saved_obj save;
	int4        color = new_color(0, 0, 0);


	save.ray = line;
	if (intersect_point(&line, objs, &save, NULL))
	{
		color = get_pixel_color(&save, objs, lights, ambient_light, helper);
		if (save.obj && save.obj->transparency)
			color = transparency(save, helper, line, objs, lights, ambient_light, color);
		if (helper.recursion > 0 && save.obj && save.obj->reflection)
		{
			int4        color2;
			int4        color_save = save.obj->color;
			int         rec = 1;
			float       reflection_coef = save.obj->reflection;

			while (1) {
				color2 = reflection(&save, helper, objs, lights, ambient_light, rec, helper.recursion);
				color2 = cut_rgb(color2, color_save);
				color_save = color2;
	//			if (save.obj && save.obj->transparency)
	//				color2 = transparency(save, helper, line, objs, lights, ambient_light, color2);
				if (!helper.magic_reflections)
					color = mult_lights(color, color2, 1, reflection_coef);
				else if (helper.magic_reflections)
					color = mult_lights(color2, color, 1, reflection_coef);
				if (rec >= helper.recursion || !save.obj || !save.obj->reflection)
					break ;
				reflection_coef *= save.obj->reflection;
				++rec;
			}
		}
	}
	return (color);
}

__kernel void   render(__global __write_only int *map, __global __read_only t_object *objs, __global __read_only t_light *lights, t_cam const cam, float ambient_light, t_helper helper) {
	const int   i = get_global_id(0);
	int4		color;
	int4		dirlight_color;
	float		y;
	float		x;

	y = i / cam.map_x;
	x = i % cam.map_x;
	color = new_color(0, 0, 0);
	if (helper.samples == 1)
		color = sample(objs, lights, cam, ambient_light, helper, y, x);
	else if (helper.samples == 2)
	{	
		color += sample(objs, lights, cam, ambient_light, helper, y - 0.25f, x);
		color += sample(objs, lights, cam, ambient_light, helper, y + 0.25f, x);
		color.x = (int)floor((float)color.x / 2.0f);
		color.y = (int)floor((float)color.y / 2.0f);
		color.z = (int)floor((float)color.z / 2.0f);
	}
	else if (helper.samples == 4)
	{
		color += sample(objs, lights, cam, ambient_light, helper, y - 0.25f, x - 0.25f);
		color += sample(objs, lights, cam, ambient_light, helper, y - 0.25f, x + 0.25f);
		color += sample(objs, lights, cam, ambient_light, helper, y + 0.25f, x - 0.25f);
		color += sample(objs, lights, cam, ambient_light, helper, y + 0.25f, x + 0.25f);
		color.x = (int)floor((float)color.x / 4.0f);
		color.y = (int)floor((float)color.y / 4.0f);
		color.z = (int)floor((float)color.z / 4.0f);
	}
	if (helper.direct_light == 1)
	{
		t_line      line = new_line(cam.point, view3d(&cam, y, x));

		dirlight_color = direct_light(&line, objs, lights);
		color = cut_rgb(color + dirlight_color, new_color(255, 255, 255));
	}
	map[i] = rgb_to_int(color);
}
