/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_context.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 14:03:42 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:12:37 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"
#include "opencllib.h"
#include <fcntl.h>

t_dev		*cl_create_device_list(void)
{
	cl_int		ret;
	t_dev		*dev;

	dev = (t_dev *)malloc(sizeof(t_dev));
	dev->platform_id = NULL;
	dev->device_cpu = NULL;
	dev->device_gpu = NULL;
	dev->num_platforms = 0;
	dev->num_devices_cpu = 0;
	dev->num_devices_gpu = 0;
	ret = clGetPlatformIDs(1, &dev->platform_id, &dev->num_platforms);
	cl_error_check(NULL, ret);
	ret = clGetDeviceIDs(dev->platform_id, CL_DEVICE_TYPE_GPU, 1,
			&dev->device_gpu, &dev->num_devices_gpu);
	cl_error_check(NULL, ret);
	ret = clGetDeviceIDs(dev->platform_id, CL_DEVICE_TYPE_CPU, 1,
			&dev->device_cpu, &dev->num_devices_cpu);
	cl_error_check(NULL, ret);
	return (dev);
}

t_cl		*cl_create_context(cl_device_id devices, cl_uint num_device)
{
	t_cl			*cl;
	cl_int			ret;

	ret = 0;
	cl = (t_cl *)malloc(sizeof(t_cl));
	cl->source_str = NULL;
	cl->device_id = NULL;
	cl->context = NULL;
	cl->command_queue = NULL;
	cl->program = NULL;
	cl->kernel = NULL;
	cl->device_id = devices;
	cl->context = clCreateContext(NULL, num_device, &devices, NULL, NULL, &ret);
	cl_error_check(cl, ret);
	cl->command_queue = clCreateCommandQueue(cl->context,
			cl->device_id, 0, &ret);
	cl_error_check(cl, ret);
	return (cl);
}

void		cl_create_program(t_cl *cl, char *filename, cl_device_id device_id)
{
	int		fd;
	int		ret;

	fd = open(filename, O_RDONLY);
	if (fd < 3)
	{
		ft_dprintf(2, "cl_err: Wrong file\n");
		exit(1);
	}
	cl->source_str = (char *)malloc(sizeof(char) * (MAX_SOURCE_SIZE + 1));
	cl->source_size = read(fd, cl->source_str, MAX_SOURCE_SIZE + 1);
	if (cl->source_size > MAX_SOURCE_SIZE)
	{
		ft_dprintf(2, "cl_err: Too large file\n");
		exit(1);
	}
	close(fd);
	cl->program = clCreateProgramWithSource(cl->context, 1,
		(const char **)&cl->source_str, (const size_t *)&cl->source_size, &ret);
	cl_error_check(NULL, ret);
	ret = clBuildProgram(cl->program, 1, &cl->device_id, NULL, NULL, NULL);
	if (ret != 0)
		print_compile_errors(cl, &device_id);
	cl_error_check(NULL, ret);
}
