/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 14:47:33 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 13:52:25 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "menu.h"

static const int	g_params_num = 6;

static const int	g_values_num = 6;

static const int	g_help_num = 17;

static char			*g_help[g_help_num] = {
	"Press W A S D Z SPACE",
	"    to move",
	"Press I J K L N M",
	"    to rotate camera",
	"Press 8 4 2 6 7 9 on numpad",
	"    to rotate object",
	"Press arrows and + -",
	"    to move object",
	"Press B",
	"    to change keyboard mode",
	"Press V",
	"    to enable direct light",
	"Use mouse",
	"    to choose object",
	"Use mouse",
	"    to rotate camera",
	"Press ESCAPE to exit"
};

static char			*g_params[g_params_num] = {
	"Magic reflection",
	"Cartoon effect",
	"Interactive mode",
	"Reflection depth",
	"Samples",
	"Chosen object type"
};

static char			*g_figures[6] = {
	"None",
	"Sphere",
	"Plane",
	"Cylinder",
	"Cone",
	"Disk"
};

static t_menu		g_values[g_params_num];

static void			init_menu_values(t_info *inf)
{
	char		*number;
	static char	plusmin[6] = "-   +\0";
	static char	plusmin2[6] = "-   +\0";

	g_values[0].value = &inf->magic_reflections;
	g_values[0].str = ONOFF(*g_values[0].value);
	g_values[1].value = &inf->effect;
	g_values[1].str = ONOFF(*g_values[1].value);
	g_values[2].value = &inf->interactive_mode;
	g_values[2].str = ONOFF(*g_values[2].value);
	g_values[3].value = &inf->recursion;
	number = ft_itoa(inf->recursion);
	ft_memcpy(plusmin + 2, number, ft_strlen(number));
	free(number);
	g_values[3].str = plusmin;
	g_values[4].value = &inf->samples;
	number = ft_itoa(inf->samples);
	ft_memcpy(plusmin2 + 2, number, ft_strlen(number));
	free(number);
	g_values[4].str = plusmin2;
	g_values[5].str = (inf->chosen_obj) ?
		g_figures[inf->chosen_obj->type] : g_figures[0];
}

int					plusmin_value(int *d, float *f, int x, int i)
{
	int		move;

	move = 1;
	if (x < g_min_place || x > (g_plus_place + g_plus_min_len) ||
				(x > (g_min_place + g_plus_min_len) && x < g_plus_place))
		return (0);
	if (x < g_plus_place)
		move = -1;
	if (d && i == 4)
		return (change_sample(d, move));
	if (d)
		*d += move;
	else if (f)
		*f += move * 0.1f;
	if (d && *d < 0)
		*d = 0;
	if (d && *d > 9)
		*d = 9;
	if (f && *f < 0)
		*f = 0;
	if (f && *f > 1)
		*f = 1;
	return (0);
}

int					helpmenu(t_info *inf)
{
	int		i;
	t_img	*menu;

	i = -1;
	menu = inf->menu;
	while (++i < menu->img_mass)
		menu->img_arr[i] = 0x505050;
	mlx_put_image_to_window(inf->mlxptr, inf->winptr, inf->menu->img_ptr, 0, 0);
	i = -1;
	while (++i < g_help_num)
		mlx_string_put(inf->mlxptr, inf->winptr, 10, 10 + i * g_skip,
				0xFFFFFF, g_help[i]);
	mlx_string_put(inf->mlxptr, inf->winptr, g_params_skip,
		inf->camera->map_y - 50, 0xFFFFFF, "Press H to see scene info");
	return (0);
}

int					fillmenu(t_info *inf, t_img *menu)
{
	int		i;

	if (inf->help_mode == 1)
	{
		helpmenu(inf);
		return (0);
	}
	init_menu_values(inf);
	i = -1;
	while (++i < menu->img_mass)
		menu->img_arr[i] = 0x505050;
	mlx_put_image_to_window(inf->mlxptr, inf->winptr, inf->menu->img_ptr, 0, 0);
	i = -1;
	while (++i < g_params_num)
		mlx_string_put(inf->mlxptr, inf->winptr,
		g_params_skip, g_params_start + i * g_skip, 0xFFFFFF, g_params[i]);
	i = -1;
	while (++i < g_values_num)
		mlx_string_put(inf->mlxptr, inf->winptr,
		g_values_skip, g_values_start + i * g_skip, 0xFFFFFF, g_values[i].str);
	if (inf->chosen_obj)
		fillmenu_obj(inf, i);
	mlx_string_put(inf->mlxptr, inf->winptr, g_params_skip,
		inf->camera->map_y - 50, 0xFFFFFF, "Press H to see keybinds");
	return (0);
}

int					triggermenu(t_info *inf, int y, int x)
{
	int		i;

	i = -1;
	if (inf->help_mode == 1)
		return (helpmenu(inf));
	fillmenu(inf, inf->menu);
	if (x >= g_menu_length)
		return (0);
	while (++i < g_values_num)
	{
		if (y >= (g_values_start + i * g_skip) &&
				y <= (g_values_start + i * g_skip + g_skip / 2))
		{
			if (i < 3 && x >= g_values_skip && x <= g_values_length)
				*g_values[i].value ^= 1;
			else if (i >= 3)
				plusmin_value(g_values[i].value, NULL, x, i);
			fillmenu(inf, inf->menu);
			main_loop(NULL);
			return (1);
		}
	}
	if (inf->chosen_obj)
		triggermenu_obj(inf, y, x);
	return (0);
}
