/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_context.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 14:03:42 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:12:27 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "opencllib.h"
#include "ft_printf.h"

int			cl_error_check(t_cl *cl, cl_int ret)
{
	if (ret != CL_SUCCESS)
	{
		ft_dprintf(2, "err: {red}OpenCL ended with code %d\n", ret);
		if (cl)
			cl_clear(cl);
		exit(1);
	}
	return (ret == CL_SUCCESS ? 0 : 1);
}

void		cl_clear(t_cl *cl)
{
	if (cl->command_queue)
		clFlush(cl->command_queue);
	if (cl->command_queue)
		clFinish(cl->command_queue);
	if (cl->kernel)
	{
		clReleaseKernel(cl->kernel);
		cl->kernel = NULL;
	}
	if (cl->program)
	{
		clReleaseProgram(cl->program);
		cl->program = NULL;
	}
	if (cl->command_queue)
	{
		clReleaseCommandQueue(cl->command_queue);
		cl->command_queue = NULL;
	}
	if (cl->context)
	{
		clReleaseContext(cl->context);
		cl->context = NULL;
	}
}

void		print_compile_errors(t_cl *cl, cl_device_id *device_id)
{
	size_t	len;
	char	*buffer;
	cl_int	ret;

	ret = CL_SUCCESS;
	len = 0;
	ret = clGetProgramBuildInfo(cl->program, *device_id,
			CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
	if (ret != CL_SUCCESS)
		exit(1);
	buffer = (char *)malloc(len * sizeof(char) + 1);
	buffer[len] = 0;
	ret = clGetProgramBuildInfo(cl->program, *device_id,
			CL_PROGRAM_BUILD_LOG, len, buffer, NULL);
	ft_printf("%s\n", buffer);
	free(buffer);
}
