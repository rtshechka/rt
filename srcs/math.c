/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 19:44:27 by bcherkas          #+#    #+#             */
/*   Updated: 2018/09/21 18:49:39 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

float	get_radian(float angle)
{
	return (M_PI * angle / 180);
}

int		sq_equation(cl_float4 k, float *res)
{
	float	t[2];
	float	discriminant;
	float	tmp;

	discriminant = pow(k.y, 2) - 4 * k.x * k.z;
	if (discriminant < 0)
		return (0);
	else if (discriminant > 0)
		discriminant = sqrt(discriminant);
	t[0] = (-k.y - discriminant) / k.x / 2;
	t[1] = (-k.y + discriminant) / k.x / 2;
	if (t[0] < 0 && t[1] < 0)
		return (0);
	*res = t[0] < t[1] ? t[0] : t[1];
	tmp = t[0] > t[1] ? t[0] : t[1];
	if (*res < tmp && *res < 0)
		*res = tmp;
	return (1);
}
