/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 14:59:31 by bcherkas          #+#    #+#             */
/*   Updated: 2018/09/12 18:04:05 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_float4		view3d(t_cam *cam, int y, int x)
{
	cl_float4		vector;

	vector.x = (-cam->map_x / 2 + x) * cam->rel_x;
	vector.y = (cam->map_y / 2 - y) * cam->rel_y;
	vector.z = cam->screen_distance;
	vector = rotate_vector(vector, cam->rotation);
	vector = vector_add(vector, cam->point);
	return (vector);
}
