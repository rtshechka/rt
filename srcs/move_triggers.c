/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_triggers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 13:02:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "objects_parser.h"
#include <math.h>

int		camera_move(int key, t_cam *camera, float move)
{
	cl_float4	pos;

	pos = new_vector(0, 0, 0);
	if (key == KEY_A || key == KEY_S || key == KEY_Z)
		move = -move;
	if (key == KEY_A || key == KEY_D)
		pos.x += move;
	else if (key == KEY_SPACE || key == KEY_Z)
		pos.y += move;
	else if (key == KEY_W || key == KEY_S)
		pos.z += move;
	else
		return (0);
	pos = rotate_vector(pos, camera->rotation);
	camera->point = vector_add(camera->point, pos);
	main_loop(NULL);
	return (1);
}

int		camera_rotate(int key, t_cam *camera, float rotate)
{
	if (key == KEY_J || key == KEY_I || key == KEY_M)
		rotate = -rotate;
	if (key == KEY_I || key == KEY_K)
		camera->rotation.x += rotate;
	else if (key == KEY_J || key == KEY_L)
		camera->rotation.y += rotate;
	else if (key == KEY_N || key == KEY_M)
		camera->rotation.z += rotate;
	else
		return (0);
	main_loop(NULL);
	return (1);
}
