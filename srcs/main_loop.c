/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 18:30:12 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 13:45:03 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "objects_parser.h"
#include <math.h>
#include <stdio.h>

void		init_args2(t_info *inf, t_helper *help)
{
	help->ambient_light_color = inf->ambient_light_color;
	help->recursion = inf->recursion;
	help->magic_reflections = inf->magic_reflections;
	help->effect = inf->effect;
	help->samples = inf->samples;
	help->direct_light = inf->direct_light;
}

void		init_args(t_info *inf)
{
	cl_int		ret;
	t_helper	help;

	clSetKernelArg(inf->cl->kernel, 1, sizeof(cl_mem),
			(const void *)&inf->mem_obj2);
	clSetKernelArg(inf->cl->kernel, 2, sizeof(cl_mem),
			(const void *)&inf->mem_obj3);
	clSetKernelArg(inf->cl->kernel, 3, sizeof(t_cam),
			(const void *)inf->camera);
	clSetKernelArg(inf->cl->kernel, 4, sizeof(float),
			(const void *)&inf->ambient_light);
	ret = clEnqueueWriteBuffer(inf->cl->command_queue,
			inf->mem_obj2, CL_TRUE, 0, inf->num_objs * sizeof(t_object),
				inf->obj_arr, 0, NULL, NULL);
	cl_error_check(inf->cl, ret);
	ret = clEnqueueWriteBuffer(inf->cl->command_queue,
			inf->mem_obj3, CL_TRUE, 0, inf->num_lights * sizeof(t_light),
				inf->light_arr, 0, NULL, NULL);
	cl_error_check(inf->cl, ret);
	init_args2(inf, &help);
	clSetKernelArg(inf->cl->kernel, 5, sizeof(t_helper), (const void *)&help);
}

void		main_loop(t_info *info)
{
	static t_info	*inf;
	cl_int			ret;
	size_t			global_item_size;

	if (info != NULL)
		inf = info;
	global_item_size = inf->num_pixels;
	init_args(inf);
	ret = clEnqueueNDRangeKernel(inf->cl->command_queue,
			inf->cl->kernel, 1, NULL, ((const size_t *)&global_item_size),
				NULL, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(inf->cl->command_queue, inf->mem_obj,
			CL_TRUE, 0, inf->num_pixels * sizeof(int),
				inf->image->img_arr, 0, NULL, NULL);
	mlx_put_image_to_window(inf->mlxptr, inf->winptr,
			inf->image->img_ptr, g_menu_length, 0);
	fillmenu(inf, inf->menu);
}
