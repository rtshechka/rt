/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersect_point.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/28 20:18:53 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 14:00:55 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "objects_parser.h"
#include <math.h>

int		choose_intersect(t_object *obj, const t_line *ray, float *t)
{
	if (obj->type == SPHERE)
		return (sphere_intersection(obj, ray, t));
	else if (obj->type == PLANE)
		return (plane_intersection(obj, ray, t));
	else if (obj->type == CYLINDER)
		return (cylinder_intersection(obj, ray, t));
	else if (obj->type == CONE)
		return (cone_intersection(obj, ray, t));
	else if (obj->type == DISK)
		return (disk_intersection(obj, ray, t));
	return (0);
}

int		intersect_point(const t_line *ray, t_object *obj, t_object **save)
{
	float		d;
	float		distance;

	*save = NULL;
	d = 0;
	distance = INFINITY;
	while (obj->type)
	{
		if (choose_intersect(obj, ray, &d) && d < distance)
		{
			distance = d;
			*save = obj;
		}
		obj++;
	}
	if (*save == NULL)
		return (0);
	return (1);
}
