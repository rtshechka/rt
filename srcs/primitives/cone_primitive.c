/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone_primitive.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:17:51 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 20:06:22 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

int			cone_intersection(const t_object *obj, const t_line *ray, float *t)
{
	float			m;
	cl_float4		k;
	const float		r_axis = ray_product(ray->ray, obj->axis);
	const float		cam_cone_axis =

	ray_product(vector_sub(ray->start, obj->center), obj->axis);
	k.x = ray->len - (1 + obj->tg) * pow(r_axis, 2);
	k.y = 2 * (ray_product(ray->ray, vector_sub(ray->start, obj->center)) -
			(obj->tg + 1) * r_axis * cam_cone_axis);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - (obj->tg + 1) *
		pow(cam_cone_axis, 2);
	if (!sq_equation(k, t))
		return (0);
	if (obj->m.y < 0 && obj->m.z < 0)
		return (1);
	m = r_axis * (*t) + cam_cone_axis;
	if ((m <= obj->m.y || obj->m.y < 0) && (m >= -obj->m.z || obj->m.z < 0))
		return (1);
	return (0);
}

int			cone_rotate(int key, t_object *obj, float move)
{
	cl_float4		tmp;

	move /= 3;
	tmp = new_vector(0, 0, 0);
	if (key == NUMPAD_8 || key == NUMPAD_6 || key == NUMPAD_9)
		move *= -1;
	if (key == NUMPAD_4 || key == NUMPAD_6)
		tmp.x += move;
	else if (key == NUMPAD_2 || key == NUMPAD_8)
		tmp.y += move;
	else if (key == NUMPAD_7 || key == NUMPAD_9)
		tmp.z += move;
	else
		return (0);
	obj->axis = ray_normalize(vector_add(obj->axis, tmp));
	main_loop(NULL);
	return (1);
}
