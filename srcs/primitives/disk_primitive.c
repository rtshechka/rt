/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disk_primitive.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 14:03:23 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 14:07:39 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

/*
** distance = -(X|V) / (D|V); X - vector from camera to plane,
** V - plane normal, D - ray vector
*/

int			disk_intersection(const t_object *obj, const t_line *ray, float *t)
{
	const float		d_v = ray_product(ray->ray, obj->normal);
	float			x_v;
	cl_float4		intersection_point;

	if (d_v == 0)
		return (0);
	x_v = ray_product(vector_sub(obj->center, ray->start), obj->normal);
	if ((d_v < 0 ? -1 : 1) != (x_v < 0 ? -1 : 1))
		return (0);
	*t = x_v / d_v;
	intersection_point = vector_add(ray->start, (ray_mult(ray->ray, *t)));
	x_v = ray_pow(vector_sub(intersection_point, obj->center));
	if (x_v <= obj->pow_radius)
		return (1);
	return (0);
}

int			disk_rotate(int key, t_object *obj, float move)
{
	cl_float4		tmp;

	tmp = new_vector(0, 0, 0);
	move /= 3;
	if (key == NUMPAD_8 || key == NUMPAD_6 || key == NUMPAD_9)
		move *= -1;
	if (key == NUMPAD_4 || key == NUMPAD_6)
		tmp.x += move;
	else if (key == NUMPAD_2 || key == NUMPAD_8)
		tmp.y += move;
	else if (key == NUMPAD_7 || key == NUMPAD_9)
		tmp.z += move;
	else
		return (0);
	obj->normal = ray_normalize(vector_add(obj->normal, tmp));
	main_loop(NULL);
	return (1);
}
