/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects_builders1.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/21 16:58:19 by bcherkas          #+#    #+#             */
/*   Updated: 2018/09/21 17:01:00 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "objects_parser.h"

t_object	new_cone(cl_float4 center, cl_float4 axis, t_rgb color,
				float angle)
{
	t_cone		*cone;
	t_object	obj;

	if (angle >= 179)
	{
		ft_printf("error: {red}Cone angle cannot be more than 178\n");
		exit(1);
	}
	cone = (t_cone *)malloc(sizeof(t_cone));
	cone->center = center;
	cone->axis = ray_normalize(axis);
	cone->tg = fpow(tan(get_radian(angle / 2)));
	obj.content = (void *)cone;
	obj.intersect = cone_intersection;
	obj.get_normal = cone_normal;
	obj.move = cone_move;
	obj.rotate = cone_rotate;
	obj.color = color;
	obj.specularity = 500;
	return (obj);
}

t_object	new_cylinder(float radius, cl_float4 center,
					cl_float4 axis, t_rgb color)
{
	t_cylinder	*cyl;
	t_object	obj;

	cyl = (t_cylinder *)malloc(sizeof(t_cylinder));
	cyl->pow_radius = fpow(radius);
	cyl->center = center;
	cyl->axis = ray_normalize(axis);
	obj.content = (void *)cyl;
	obj.intersect = cylinder_intersection;
	obj.get_normal = cyl_normal;
	obj.move = cyl_move;
	obj.rotate = cyl_rotate;
	obj.color = color;
	obj.specularity = 500;
	return (obj);
}

t_object	new_plane(cl_float4 point, cl_float4 normal, t_rgb color)
{
	t_plane		*plane;
	t_object	obj;

	plane = (t_plane *)malloc(sizeof(t_plane));
	plane->point = point;
	plane->normal = ray_normalize(normal);
	obj.content = (void *)plane;
	obj.intersect = plane_intersection;
	obj.get_normal = plane_normal;
	obj.move = plane_move;
	obj.rotate = plane_rotate;
	obj.color = color;
	obj.specularity = -1;
	return (obj);
}

t_object	new_sphere(float radius, cl_float4 center, t_rgb color)
{
	t_sphere	*sphere;
	t_object	obj;

	sphere = (t_sphere *)malloc(sizeof(t_sphere));
	sphere->pow_radius = fpow(radius);
	sphere->center_point = center;
	obj.content = (void *)sphere;
	obj.intersect = sphere_intersection;
	obj.get_normal = sphere_normal;
	obj.move = sphere_move;
	obj.rotate = sphere_rotate;
	obj.color = color;
	obj.specularity = 500;
	return (obj);
}
