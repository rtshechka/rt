/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere_primitive.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:17:51 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/19 18:39:29 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

int		sphere_intersection(const t_object *obj, const t_line *ray, float *t)
{
	const cl_float4		cam_sphere = vector_sub(ray->start, obj->center);
	cl_float4			k;

	k.x = ray->len;
	k.y = 2 * ray_product(ray->ray, cam_sphere);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - obj->pow_radius;
	if (!sq_equation(k, t))
		return (0);
	return (1);
}

int		sphere_rotate(int key, t_object *obj, float move)
{
	if (obj && move && key)
		return (0);
	return (0);
}
