/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/28 20:20:47 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 20:19:19 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "objects_parser.h"
#include <math.h>

int		mousepress(int button, int x, int y, void *p)
{
	t_info *const	inf = (t_info *)p;
	cl_float4		point3d;
	t_line			line;
	int				ret;
	t_object		*obj;

	if (button != 1)
		return (0);
	if (x >= g_menu_length && inf->interactive_mode)
	{
		x -= g_menu_length;
		point3d = view3d(inf->camera, y, x);
		line = new_line(inf->camera->point, point3d);
		ret = intersect_point(&line, inf->obj_arr, &obj);
		if (ret == 1)
			inf->chosen_obj = obj;
		else
			inf->chosen_obj = NULL;
	}
	triggermenu(inf, y, x);
	return (1);
}

void	move_obj(t_info *inf, cl_float4 vector, t_object *obj)
{
	if (!inf->interactive_mode || inf->chosen_obj == NULL)
		return ;
	vector = rotate_vector(vector, inf->camera->rotation);
	obj->center = vector_add(obj->center, vector);
	main_loop(NULL);
	return ;
}

int		mouse_move(int x, int y, void *p)
{
	t_info *const	inf = (t_info *)p;
	static int		xy[2] = {-1000, -1000};

	if (xy[0] == -1000 && xy[1] == -1000)
	{
		xy[0] = x;
		xy[1] = y;
		return (0);
	}
	if (!inf->interactive_mode)
	{
		inf->camera->rotation.y -= (xy[0] - x) * MOUSEMOVE;
		inf->camera->rotation.x -= (xy[1] - y) * MOUSEMOVE;
	}
	xy[0] = x;
	xy[1] = y;
	if (!inf->interactive_mode)
		main_loop(NULL);
	return (1);
}
