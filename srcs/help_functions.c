/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 20:02:23 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static const int	g_arr_startlen = 16;

void		del_obj(void *ptr, size_t size)
{
	t_object	*obj;

	obj = (t_object *)ptr;
	if (size)
		obj = (t_object *)ptr;
	if (obj)
		free(obj);
}

void		errmsg(const char *str)
{
	ft_printf("error: {red}%s\n", str);
	exit(1);
}

cl_int4		new_color(int r, int g, int b)
{
	cl_int4	rgb;

	rgb.x = r;
	rgb.y = g;
	rgb.z = b;
	return (rgb);
}

static void	convert_lights(t_list *light, t_info *inf)
{
	int		i;
	t_list	*tmp;

	i = 0;
	tmp = light;
	inf->num_lights = ft_lstlen(light) + 1;
	if (!(inf->light_arr = (t_light *)ft_memalloc(sizeof(t_light) *
					(inf->num_lights))))
		errmsg("Cannot allocate enough memory");
	while (light)
	{
		inf->light_arr[i] = *((t_light *)light->content);
		light = light->next;
		i++;
	}
	inf->light_arr[i].type = 0;
	ft_lstdel(&tmp, ft_lstdelcontent);
}

void		convert_to_array(t_list *obj, t_list *light, t_info *inf)
{
	size_t			i;
	t_list			*tmp;

	i = 0;
	tmp = obj;
	inf->num_objs = (1 + (ft_lstlen(obj) / g_arr_startlen)) * g_arr_startlen;
	if (!(inf->obj_arr = (t_object *)ft_memalloc(sizeof(t_object) *
					(inf->num_objs + 1))))
		errmsg("Cannot allocate enough memory");
	while (obj)
	{
		inf->obj_arr[i] = *((t_object *)obj->content);
		obj = obj->next;
		i++;
	}
	inf->obj_arr[i].type = 0;
	ft_lstdel(&tmp, ft_lstdelcontent);
	convert_lights(light, inf);
}
