/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 14:56:41 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include "parser.h"
#include <time.h>

void		parse_scene(t_info *inf, char *str)
{
	t_file	*file;

	file = parsing(str);
	convert_to_array(file->obj_stack, file->light_stack, inf);
	inf->camera = file->camera;
	inf->scene_name = file->scene_name;
	inf->ambient_light = file->ambient_light;
	inf->ambient_light_color = file->ambient_light_color;
	inf->camera->map_x = file->scene_size_x;
	inf->camera->map_y = file->scene_size_y;
	inf->num_pixels = inf->camera->map_x * inf->camera->map_y;
	inf->camera->screen_distance = 1;
	inf->camera->screen_width = 1;
	inf->camera->screen_height = (float)file->scene_size_x / file->scene_size_y;
	inf->camera->rel_x = inf->camera->screen_height / (float)inf->camera->map_x;
	inf->camera->rel_y = inf->camera->screen_width / (float)inf->camera->map_y;
	inf->recursion = file->reflection;
	inf->magic_reflections = file->magic_reflections;
	inf->effect = file->effect;
	free(file);
}

void		init_inf(t_info *inf)
{
	inf->camera = NULL;
	inf->obj_arr = NULL;
	inf->light_arr = NULL;
	inf->image = NULL;
	inf->mlxptr = NULL;
	inf->winptr = NULL;
	inf->chosen_obj = NULL;
	inf->interactive_mode = 1;
	inf->magic_reflections = 0;
	inf->help_mode = 0;
	inf->samples = 1;
	inf->direct_light = 0;
}

void		cl_start(t_info *inf, t_cl **cl)
{
	t_dev	*dev;
	cl_int	ret;

	ret = CL_SUCCESS;
	dev = cl_create_device_list();
	*cl = cl_create_context(dev->device_gpu, dev->num_devices_gpu);
	cl_create_program(*cl, "srcs/render.cl", dev->device_gpu);
	inf->mem_obj = clCreateBuffer((*cl)->context, CL_MEM_WRITE_ONLY,
			inf->num_pixels * sizeof(int), NULL, &ret);
	inf->mem_obj2 = clCreateBuffer((*cl)->context, CL_MEM_READ_ONLY,
			inf->num_objs * sizeof(t_object), NULL, &ret);
	inf->mem_obj3 = clCreateBuffer((*cl)->context, CL_MEM_READ_ONLY,
			inf->num_lights * sizeof(t_object), NULL, &ret);
	cl_error_check(*cl, ret);
	(*cl)->kernel = clCreateKernel((*cl)->program, "render", &ret);
	if (ret != CL_SUCCESS)
	{
		clReleaseMemObject(inf->mem_obj);
		clReleaseMemObject(inf->mem_obj2);
		clReleaseMemObject(inf->mem_obj3);
		cl_error_check(*cl, ret);
	}
	clSetKernelArg((*cl)->kernel, 0, sizeof(cl_mem), (void *)&inf->mem_obj);
	free(dev);
}

int			main(int ac, char **av)
{
	t_info	inf;

	if (ac != 2)
	{
		ft_printf("usage: {green} ./RTv1 \"scene.rt\"\n");
		exit(1);
	}
	init_inf(&inf);
	parse_scene(&inf, av[1]);
	mlx_starts(triggers, &inf);
	mlx_mouse_hook(inf.winptr, mousepress, (void *)&inf);
	mlx_hook(inf.winptr, 6, 0, mouse_move, (void *)&inf);
	cl_start(&inf, &(inf.cl));
	fillmenu(&inf, inf.menu);
	main_loop(&inf);
	mlx_loop(inf.mlxptr);
}
