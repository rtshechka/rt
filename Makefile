.PHONY: all clean fclean re pclean pre

UNAME=$(shell uname)

CC=clang
WERR=-Wall -Wextra -Werror
INCLUDES=-Ilibft/includes -Iincludes
NAME=RT
OBJDIR=objs
SRCDIR=srcs

OPENCL=create_context.c opencl_error.c

VECTOR=vector_ariphmetic.c vector_operations.c vector.c vector_rotate.c vector_operations1.c

PRIMITIVES=sphere_primitive.c plane_primitive.c cylinder_primitive.c cone_primitive.c disk_primitive.c

MLXI=mlx_wrapper.c triggers.c move_triggers.c mouse.c

MAIN=main.c main_loop.c camera.c help_functions.c math.c intersect_point.c menu.c menu_object.c

PARSER=array_funcs.c parsing.c str_funcs.c sort.c \
	   objects_parser.c objects_parser_help.c \
	   parse_cone.c parse_cylinder.c parse_plane.c parse_sphere.c parse_disk.c \
	   light_parser.c light_parser_help.c \
	   parse_light_ambient.c parse_light_direct.c \
	   scene_parser.c scene_parser_help.c

SRC= $(MAIN) $(MLXI) $(addprefix primitives/, $(PRIMITIVES)) $(addprefix vector/, $(VECTOR)) $(addprefix parser/, $(PARSER)) $(OPENCL)

OBJ=$(SRC:.c=.o)
LIBFT=libft/libft.a

MLXINCLUDES=-framework OpenGL -framework AppKit -I/usr/local/include -L/usr/local/lib -lmlx -framework OpenCL

all: $(OBJDIR) $(NAME)

$(OBJDIR):
	@echo "Compiling Raytracer"
	@mkdir -p objs/vector objs/primitives objs/parser

SUBMODULE:

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBFT)
	@$(CC) $(WERR) $(INCLUDES) $(MLXINCLUDES) -o $@ $^ -g
	@echo ""
	@echo "Raytracer compiled"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@printf "."
	@$(CC) $(WERR) $(INCLUDES) -c -o $@ $< -g

debug:
	$(CC) $(WERR) $(INCLUDES) $(MLXINCLUDES) $(addprefix $(SRCDIR)/, $(SRC))  libft/libft.a -o $(NAME) -g -lpthread

$(LIBFT):
	@echo ""
	@$(MAKE) --no-print-directory -C libft

clean:
	@$(MAKE) --no-print-directory -C libft clean
	@rm -rf $(OBJDIR)
	@echo "cleaned"

fclean:
	@$(MAKE) --no-print-directory -C libft fclean
	@rm -rf $(OBJDIR)
	@rm -f $(NAME)
	@echo "fcleaned"

re: fclean all
